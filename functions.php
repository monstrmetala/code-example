<?php
define('THEME_DATA_FORMAT', 'F j, Y');

include_once 'lib/svg-icons.php';
include_once 'lib/functions_default.php';
include_once 'lib/functions_enqueue.php';
include_once 'lib/functions_menu.php';
include_once 'lib/post_types.php';

include_once 'lib/functions-ajax.php';
include_once 'lib/widgets.php';
include_once 'lib/acf.php';

/**
 *      More includes ..
 */
include_once 'lib/advanced_functions.php';


/**
 * 	Rename Image before upload "new name" is md5( 'old name' )
 */
add_filter('sanitize_file_name', 'rename_img_before_upload', 99 );
function rename_img_before_upload( $filename ){
	$pi = pathinfo($filename);

	if(
		isset( $pi['filename'] ) && !empty( $pi['filename'] ) &&
		isset( $pi['extension'] ) && !empty( $pi['extension'] )
	){

		$new_filename = md5( $pi['filename'] );
		$new_filename .= '.'.$pi['extension'];
		return $new_filename;

	}else return $filename;
}

function render_graph () {
	the_field('graph_script', 'option');
}

function get_prev_next ( )
{
	$ID = get_the_ID();

	$prev_post = null;
	$next_post = null;

	$lessons_list_base = get_field( 'lessons_list_base', 'option');
	foreach ($lessons_list_base as $key => $value) {

		if ($value->ID == $ID) {
			$prev_post = (isset($lessons_list_base[$key - 1])) ? $lessons_list_base[$key - 1] : null;
			$next_post = (isset($lessons_list_base[$key + 1])) ? $lessons_list_base[$key + 1] : null;
		}
	}

	$lessons_list_pro = get_field( 'lessons_list_pro', 'option');
	foreach ($lessons_list_pro as $key => $value) {

		if ($value->ID == $ID) {
			$prev_post = (isset($lessons_list_pro[$key - 1])) ? $lessons_list_pro[$key - 1] : null;
			$next_post = (isset($lessons_list_pro[$key + 1])) ? $lessons_list_pro[$key + 1] : null;
		}
	}

	$tpl = '<a class="leason-navi__item %s" href="%s" title="%s" ><b>%s</b><span>%s</span></a>';

	if ($prev_post) {
		$prev_post = sprintf($tpl,
			'leason-navi__item--prev',
			get_permalink($prev_post->ID),
			esc_attr($prev_post->post_title),
			'Предидущий урок',
			$prev_post->post_title
		);
	}

	if ($next_post) {
		$next_post = sprintf($tpl,
			'leason-navi__item--next',
			get_permalink($next_post->ID),
			esc_attr($next_post->post_title),
			'Следующий урок',
			$next_post->post_title
		);
	}

	return [
		'prev' => $prev_post,
		'next' => $next_post
	];

}
