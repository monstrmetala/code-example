<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#fff!important">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width">
	<title></title>
	<style>
		@media only screen {
			html {
				min-height: 100%;
				background: #fff;
				padding: 30px;
			}
		}

		@media only screen and (max-width:380px) {
			.small-float-center {
				margin: 0 auto !important;
				float: none !important;
				text-align: center !important
			}
		}

		@media only screen and (max-width:380px) {
			table.body img {
				width: auto;
				height: auto
			}

			table.body center {
				min-width: 0 !important
			}

			table.body .container {
				width: 95% !important
			}

			table.body .columns {
				height: auto !important;
				-moz-box-sizing: border-box;
				-webkit-box-sizing: border-box;
				box-sizing: border-box;
				padding-left: 16px !important;
				padding-right: 16px !important
			}

			table.body .columns .columns {
				padding-left: 0 !important;
				padding-right: 0 !important
			}

			table.body .collapse .columns {
				padding-left: 0 !important;
				padding-right: 0 !important
			}

			th.small-4 {
				display: inline-block !important;
				width: 33.33333% !important
			}

			th.small-6 {
				display: inline-block !important;
				width: 50% !important
			}

			th.small-8 {
				display: inline-block !important;
				width: 66.66667% !important
			}

			th.small-12 {
				display: inline-block !important;
				width: 100% !important
			}

			.columns th.small-12 {
				display: block !important;
				width: 100% !important
			}

			table.menu {
				width: 100% !important
			}

			table.menu td,
			table.menu th {
				width: auto !important;
				display: inline-block !important
			}

			table.menu.vertical td,
			table.menu.vertical th {
				display: block !important
			}

			table.menu[align=center] {
				width: auto !important
			}
		}

		@media (min-width:597px) {
			th.large-6.first {
				padding-left: 0
			}

			th.large-6.last {
				padding-right: 0
			}
		}

		@media (max-width: 480px) {
			.second-block-table {
				text-align: left !important;
			}
		}
		p.p-msg {
			margin: 0px 0px 15px; font-stretch: normal; font-size: 10px; font-family: Helvetica, Arial; line-height: 1.3;
		}
		body {
			-moz-box-sizing:border-box;
			-ms-text-size-adjust:100%;
			-webkit-box-sizing:border-box;
			-webkit-text-size-adjust:100%;
			Margin:0;
			background:#fff!important;
			box-sizing:border-box;
			color:#0a0a0a;
			font-family:Helvetica,Arial,sans-serif;
			font-size:16px;
			font-weight:400;
			line-height:1.3;
			margin: 25px;
			min-width:100%;
			padding:0;
			text-align:left;
			width:100%!important
		}
		.box {
			color: rgb(0, 0, 0);
			font-family: Helvetica;
			font-size: 12px;
      font-style: normal;
			font-variant-caps: normal;
			font-weight: normal;
      letter-spacing: normal;
			text-align: start;
			text-indent: 0px;
      text-transform: none;
			white-space: normal;
			word-spacing: 0px;
      -webkit-text-stroke-width: 0px;
		}
	</style>
</head>

<body>
	<div class="box">

			<p class="p-msg"><a href="{{pdf_url}}" title="Title" >Here</a> is an approximate breakdown of the hours and costs involved to get your software thoroughly tested and future-proofed against severe bugs.</p>
			<p class="p-msg">A quick disclaimer: While we tried to make our quote calculator as precise as possible, it only generates a rough estimation.</p>
			<p class="p-msg">Would it be unreasonable for you to carve out 15 minutes of your time to find out if and how we can help you? <a href="{{book_url}}" target="_blank" title="Book a call" >&lt;&lt;Book a call&gt;&gt;</a></p>
			<p class="p-msg">After our Zoom meeting, we’ll follow you up with the final project scope estimation, timeline, and pricing. </p>

			<p class="p-msg">Hope this was useful and looking forward to speaking with you soon.</p>
			<p class="p-msg">Thank you so much, <br>Konst</p>
			<p class="p-msg">P.S. Whenever you are ready, here are 3 ways we can help you enhance your product quality so that you can scale your business. </p>
			<br>
			<p class="p-msg"><strong>1. Prepare your product for launch and decrease time to market</strong></p>
			<p class="p-msg">Did you know that fixing software bugs in post-production is 15 times costlier than eliminating them early on? See how we helped our fintech client successfully launch their product and get recognized by Mastercard. <a href="https://qawerk.com/case-studies/zazu-mobile-solution-for-managing-finances/?utm_source=mail&utm_medium=calculator&utm_campaign=quote-calculator" title="Show Me" >&lt;&lt;Show Me&gt;&gt;</a></p>
			
			<p class="p-msg"><strong>2. Release software updates frequently and safely</strong></p>
			<p class="p-msg">The user demand for new features hits all-time highs, yet integrating a new functionality is never a smooth process, often resulting in new bugs popping up at the worst time. Cover your product with base regression, double check usability, and stress-test your app. See how we helped Unfold boost the app’s quality and get acquired by Squarespace. <a href="https://qawerk.com/case-studies/unfold-mobile-toolkit-for-storytellers/?utm_source=mail&utm_medium=calculator&utm_campaign=quote-calculator" title="Click here" >&lt;&lt;Click here&gt;&gt;</a></p>

			<p class="p-msg"><strong>3. Create and update documentation for your project</strong></p>
			<p class="p-msg">Reduce revisions and miscommunication with crisp and easily digestible technical copy. Our technical writers will help you prepare professional-looking test plans, test cases, test designs, bug reports, use cases, and checklists. <a href="https://qawerk.com/services/documentation-design-technical-writing/?utm_source=mail&utm_medium=calculator&utm_campaign=quote-calculator" title="Click here" >&lt;&lt;Click here&gt;&gt;</a>  to learn more.  </p>

		<p class="p-msg">
			<span class="name" style="-webkit-text-size-adjust:none; font: 14px Helvetica, Arial; color: #000000;">
				<b>Konstantin Klyagin</b>
			</span>
			<br>
			<span style="-webkit-text-size-adjust:none; font: bold 11px Helvetica, Arial; color: #000000;">Founder and CEO at QAwerk</span>
			<br>
		</p>

		<a href="https://www.qawerk.com/" target="_blank"  style="font-stretch: normal; font-size: 10px; line-height: normal; font-family: Helvetica, Arial;">
			<img src="https://qawerk.com/wp-content/uploads/2020/07/qawerk_logo_500.png" alt="Qawerk" style="-webkit-text-size-adjust:none; width: 120px; height: auto;" width="120">
		</a>
		<br>
		<span style="font-stretch: normal; font-size: 11px; line-height: normal; font-family: Helvetica, Arial;">
			Quality Software Testing Service<br>
		</span>
	</div>
</body>

</html>
