<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#fff!important">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width">
	<title></title>
	<style>
		html {
			min-height: 100%;
			background: #fff;
			padding: 30px;
		}
		
		p.p-msg {
			margin: 0px 0px 15px; font-stretch: normal; font-size: 14px; font-family: Helvetica, Arial; line-height: 1.3;
		}
		body {
			-moz-box-sizing:border-box;
			-ms-text-size-adjust:100%;
			-webkit-box-sizing:border-box;
			-webkit-text-size-adjust:100%;
			Margin:0;
			background:#fff!important;
			box-sizing:border-box;
			color:#0a0a0a;
			font-family:Helvetica,Arial,sans-serif;
			font-size:16px;
			font-weight:400;
			line-height:1.3;
			margin: 25px;
			min-width:100%;
			padding:0;
			text-align:left;
			width:100%!important
		}
		.box {
			color: rgb(0, 0, 0);
			font-family: Helvetica;
			font-size: 16px;
      font-style: normal;
			font-variant-caps: normal;
			font-weight: normal;
      letter-spacing: normal;
			text-align: start;
			text-indent: 0px;
      text-transform: none;
			white-space: normal;
			word-spacing: 0px;
      -webkit-text-stroke-width: 0px;
		}
	</style>
</head>

<body>
	<div class="box">

		<p class="p-msg"><a href="{{pdf_url}}" title="Link to PDF" >Here</a> is an approximate breakdown of the hours and costs involved to get your software thoroughly tested and future-proofed against severe bugs.</p>
		<strong>From: </strong> <a href="mailto:{{client_email}}">{{client_email}}</a><br>
		<strong>Sender's IP:</strong> {{ip}}<br>
		<strong>Sender's Country:</strong> {{contry}}<br>
		<strong>Page: </strong>{{page_title}}<br>
		<strong>URL: </strong><a href="{{page_url}}" target="_blank">{{page_url}}</a><br>
		--
		<p>This e-mail was sent from a Quote Calculator Plugin - QAwerk (https://qawerk.com)</p>

	</div>
</body>

</html>
