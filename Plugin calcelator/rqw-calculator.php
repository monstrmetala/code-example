<?php
/*
Plugin Name: RW/QA Calculator
Plugin URI: https://qawerk.com/
Description: Plugin for calculating the cost of testing sites and web applications. Use [rw_calculator] shortcode for display calculator.
Version: 0.1
Author: RW Dragon
*/

define('CLC_PATH', __DIR__);
define('CLC_URL', plugin_dir_url( __FILE__ ) );

define('PDF_LIB', CLC_PATH . '/PDF/html2pdf' );
define('PDF_FILES', CLC_PATH . '/PDF/files' );
// define('PDF_IMGS', CLC_URL . 'PDF/images/' );
define('PDF_IMGS', 'https://darkness.zp.ua/pdf/rw-img/' );

if (!file_exists( PDF_FILES )) {
  $is_create_folder = mkdir(PDF_FILES, 0777, true);
}

include_once CLC_PATH . '/inc/plugin-option-page.php';
include_once CLC_PATH . '/inc/post-type.php';

include_once CLC_PATH . '/inc/assets.php';
include_once CLC_PATH . '/inc/svg-icons.php';

include_once CLC_PATH . '/build_calculator.php';
include_once CLC_PATH . '/inc/shortcodes.php';
include_once CLC_PATH . '/inc/ajax.php';
include_once CLC_PATH . '/inc/e-mails.php';


function field_handler_array($answer) {
	$res = false;

	if (strpos($answer['calc'], ',') !== false){
		$res = [];

		$qa = explode(',', $answer['calc']);
		if (is_array($qa) && count($qa) == 2) {
			$min = (float)$qa[0];
			$max = (float)$qa[1];
			$mid = ($min + $max)/2;
			
			$res['min'] = $min;
			$res['max'] = $max;
			$res['mid'] = $mid;
		}
	}

	return $res;
}
