<?php
global $SVG;

function rw_calc_add_class_to_body( $classes ) {
	$classes[] = 'has-rw-calculator';
	return $classes;
}


function build_calculator() {
	add_filter( 'body_class','rw_calc_add_class_to_body' );

  include_once CLC_PATH . '/configs/calc_config.php';
  do_action('before_rw_calculator');
	?>
	<div class="rw-calculator" data-rw-calculator >
    <form action="" method="post" class="rqw-calc" data-rw-calculator-form >
      
      <?php 
        foreach ($config as $key_step => $step) {
          render_step($step, $key_step);
        }
      ?>

      <div class="rqw-actions rqw-actions--next" data-rw-next>
        <button class="calc-btn" type="button" data-rw-next-btn>Next question</button>
      </div>

      <div class="rqw-actions rqw-actions--submit" data-rw-submit>
        <button class="calc-btn calc-btn--submit" type="submit">GET QUOTE</button>
      </div>

    </form>
  </div>

  <?php include_once CLC_PATH . '/tpl/time-to-test.php'; ?>
  <?php include_once CLC_PATH . '/tpl/quote-popup.php'; ?>

  <?php
  do_action('after_rw_calculator');
  
}

function render_step( $step, $key_step ) {
  ?>
  <div class="rqw-step" >
    <div class="rqw-step__box">
			<?php 
				if (!empty($step['title'])) {
					printf( '<p class="rqw-step__title">%s</p>', do_shortcode( $step['title'] ) );
				}
				
        foreach ($step['qss'] as $key_quest => $quest) {
          render_step_question($quest, $key_quest, $key_step);
        }
      ?>
    </div>
  </div>
  <?php
}

function render_step_question ($quest, $key_quest, $key_step) {
	// echo '<pre>$key_quest<br />'; var_dump($key_quest); echo '</pre>';
	// echo '<pre>$quest<br />'; var_dump($quest); echo '</pre>';
	global $SVG;
  $type = $quest['type'];

	$qest_classes = 'quest-id__' . $key_quest;
	if ('q-step1-0' == $key_quest) {
		$qest_classes .= ' rqw-quest--first-visible ';
	}

	$quest_trigger = '';
	if (isset($quest['trigger_open'])) {
		$quest_trigger = sprintf('data-quest-trigger-open="%s"', $quest['trigger_open'] );
	}


	$has_all = '';
	if (isset($quest['has_all'])) {
		$has_all = 'data-quest-has-all';
	}

  ?>
  <div class="rqw-quest <?php echo $qest_classes; ?>" <?php echo $quest_trigger; ?> data-quest-key="<?php echo $key_quest; ?>" <?php echo $has_all; ?>>
    <h4 class="rqw-quest__title"><?php echo $quest['q']; ?></h4>
    <div class="rqw-quest__answs">
      <?php 
        if ($type != 'number')
        foreach ($quest['a'] as $key_a => $ans) {
          $name = $key_quest;
          $value = $key_a;

          $trigger_open = '';
          if (isset($ans['trigger_open'])) {
            $trigger_open = sprintf('data-trigger-open="%s"', $ans['trigger_open'] );
          }
          
          $icon = '';
          if (isset($ans['icon']) && !empty($ans['icon']) ) {
            $icon = $SVG[$ans['icon']];
          }

					$answer_type = '';
					if (isset($ans['answer-type']) && 'checkbox' == $type ) {
						
						switch ($ans['answer-type']) {
							case 'all':
								$answer_type = 'data-answer-type="all"';
							break;
						}
					} 

          ?>
            <label class="answer">
              <input type="<?php echo $type ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>" <?php echo $trigger_open; ?> <?php echo $answer_type; ?> > 
              <span class="answer__icon"><?php echo $icon; ?></span>
              <span class="answer__label"><?php echo $ans['text']; ?></span>
              <span class="answer__checked"></span>
            </label>
          <?php 
        }
        if ($type == 'number') {
					$name = $key_quest;
					
					$trigger_open = '';
          if (isset($ans['trigger_open'])) {
            $trigger_open = sprintf('data-trigger-range-open="%s"', $ans['trigger_open'] );
          }
					
          ?>
            <label class="rqw-range" data-rw-calculator-range >
              <span>0</span>
              <input type="range" name="<?php echo $name; ?>" min="0" max="5" value="0" step="1" <?php echo $trigger_open; ?> >
            </label>
          <?php
        }
      ?>
    </div>
  </div>
  <?php
}
