<?php 
  global $SVG;
	$rwclc_options = get_option('rwcalc');

  $meetz = '';
  if (isset($rwclc_options['meet_url']) && !empty($rwclc_options['meet_url'])) {
    $meetz = sprintf('<a href="%s" title="Book Meeting" class="btn-meeting" target="_blank">Book Meeting</a>', $rwclc_options['meet_url'] );
  }
/*
<input id="rwcalc-popup-trigger" type="checkbox" class="rwcalc-popup__trigger" hide date-rw-calculator-popup-trigger >
*/ 
?>
<input id="rwcalc-popup-trigger" type="checkbox" class="rwcalc-popup__trigger" hide date-rw-calculator-popup-trigger >
<div class="rwcalc-popup" >
  <div class="rwcalc-popup__body rwc-grid  has--plan" date-rw-calculator-popup>
    <label for="rwcalc-popup-trigger" class="rwcalc-popup__close" title="Close Popup">
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" width="29" height="29" viewBox="0 0 29 29"><path stroke="#000" stroke-opacity=".3" d="M7 7l14.2 14.2M21.2 7L7.1 21.3"/></svg>
    </label>

    <div class="rwc-grid__plan">
      <div class="rwc-plan">
        <strong>BTW, looks like this plan fits you</strong>
        <div class="rwc-plan__card" data-rwc-plan>
          <div class="rwc-plan__ico"><?php echo $SVG['svg-plan-1']; ?></div>
          <span class="rwc-plan__subtitle">Manual</span>
          <span class="rwc-plan__name">Basic</span>
          <span class="rwc-plan__price">$450</span>

          <ul class="rwc-plan__info">
            <li>1 software type </li>
            <li>4 devices </li>
            <li>2 OS </li>
            <li>up to 3 browsers</li>
          </ul>

        </div>
      </div>
    </div>

    <div class="rwc-grid__estiname">
      <div class="rwc-est">
        <h2 class="rwc-est__title"><?php echo $SVG['svg-clock']; ?>Rough Testing Estimate</h2>
        <p class="rwc-est__text">This is an approximate estimate. Exact hours depend<br> on bugs context and severity</p>

        <div class="quote-hdm">
          <span class="quote-hdm__label">Optimistic</span>
          <p class="quote-hdm__time quote-hdm__time--hours" data-rwc-ppp-opt-h ><b>150</b> hours</p>
          <p class="quote-hdm__time quote-hdm__time--days" data-rwc-ppp-opt-d ><b>59.8</b> days</p>
          <p class="quote-hdm__time quote-hdm__time--month" data-rwc-ppp-opt-m ><b>2</b> months</p>
        </div>

        <div class="quote-hdm">
          <span class="quote-hdm__label">Pessimistic</span>
          <p class="quote-hdm__time quote-hdm__time--hours" data-rwc-ppp-pes-h ><b>1</b> hours</p>
          <p class="quote-hdm__time quote-hdm__time--days" data-rwc-ppp-pes-d ><b>2</b> days</p>
          <p class="quote-hdm__time quote-hdm__time--month" data-rwc-ppp-pes-m ><b>3</b> months</p>
        </div>

      </div>
    </div>

    <div class="rwc-grid__form">
      <div class="rwc-form">
        <h2 class="rwc-form__title">Get a detailed breakdown and pricing to your email inbox</h2>
        <form action="" class="rwc-form__form" data-rwc-pdf-form >
          <input type="email" name="email" placeholder="Enter email*" required>
					<input type="hidden" name="ip" data-set-geo="ip">
					<input type="hidden" name="country" data-set-geo="country">
					<input type="hidden" name="page_name" value="<?php echo esc_attr( get_the_title() ); ?>">
					<input type="hidden" name="page_url" value="<?php echo esc_attr( get_the_permalink() ); ?>" >
          <button type="submit" class="">Get Breakdown</button>
        </form>

        <p class="rwc-form__meet">
          <span>Or book a meeting to discuss your project right away</span>
          <?php echo $meetz; ?>
        </p>
      </div>
    </div>

    <div class="rwc-grid__img">
      <?php 
        $path = CLC_PATH . '/assets/img/ppp-img.svg';
        if (file_exists($path)) {
          printf('<img src="%s" alt="Popup Mockup">', CLC_URL . '/assets/img/ppp-img.svg' );
        }
      ?>
    </div>

  </div>
  <label for="rwcalc-popup-trigger"></label>
</div>

