<?php
  global $settings_page;
  
  $settings_page = [];
  $settings_page['Packeges'] = [];

  $settings_page['Packeges'][] = [
    'slug' => 'basic_packege',
    'name' => '"Basic" packege',
    'type' => 'pack',
    'pack' => [
      // ['slug' => 'ico',   'placeholder' => 'Icon Url',  'type' => 'url' ],
      ['slug' => 'name',  'placeholder' => 'Name',      'type' => 'text' ],
      ['slug' => 'price', 'placeholder' => 'Price',     'type' => 'number' ],
      ['slug' => 'info',  'placeholder' => 'Info',      'type' => 'textarea' ],
    ]
  ];

  $settings_page['Packeges'][] = [
    'slug' => 'standart_packege',
    'name' => '"Standard" packege',
    'type' => 'pack',
    'pack' => [
      ['slug' => 'name',  'placeholder' => 'Name',      'type' => 'text' ],
      ['slug' => 'price', 'placeholder' => 'Price',     'type' => 'number' ],
      ['slug' => 'info',  'placeholder' => 'Info',      'type' => 'textarea' ],
    ]
  ];

  $settings_page['Packeges'][] = [
    'slug' => 'pro_packege',
    'name' => '"Pro" packege',
    'type' => 'pack',
    'pack' => [
      ['slug' => 'name',  'placeholder' => 'Name',      'type' => 'text' ],
      ['slug' => 'price', 'placeholder' => 'Price',     'type' => 'number' ],
      ['slug' => 'info',  'placeholder' => 'Info',      'type' => 'textarea' ],
    ]
  ];

  $settings_page['Packeges'][] = [
    'slug' => 'ultimate_packege',
    'name' => '"Ultimate" packege',
    'type' => 'pack',
    'pack' => [
      ['slug' => 'name',  'placeholder' => 'Name',      'type' => 'text' ],
      ['slug' => 'price', 'placeholder' => 'Price',     'type' => 'number' ],
      ['slug' => 'info',  'placeholder' => 'Info',      'type' => 'textarea' ],
    ]
  ];

  $settings_page['Popup'] = [];
  $settings_page['Popup'][] = [ 'slug' => 'meet_url', 'name' => 'Meet Url', 'placeholder' => 'Meet Page Url', 'type' => 'url' ];

	$settings_page['PDF'] = [];
	$settings_page['PDF'][] = [ 'slug' => 'rate', 'name' => 'Rate', 'placeholder' => '$ per hour', 'type' => 'number' ];
	$settings_page['PDF'][] = [ 'slug' => 'redirect_url', 'name' => 'Redirect URL (after form submit)', 'placeholder' => 'URL', 'type' => 'url' ];
	
	$settings_page['Emails'] = [];
	$settings_page['Emails'][] = [ 'slug' => 'admin_email', 'name' => 'Admin Email', 'placeholder' => 'Email', 'type' => 'email' ];


