<?php

$config = [
  'step1' => [
    'qss' =>  [
      'q-step1-0' => [
        'q' => 'What does your software product consist of?',
        'a' => [
          'a-step1-0-0' => ['text' => 'Mobile', 	'icon' => 'svg-mobile',	'next' => 'step21' ],
          'a-step1-0-2' => ['text' => 'Web',			'icon' => 'svg-web',		'next' => 'step23' ],
          'a-step1-0-1' => ['text' => 'Desktop',	'icon' => 'svg-desktop','next' => 'step22' ],
          'a-step1-0-3' => ['text' => 'Backend',	'icon' => 'svg-backend','next' => 'step24' ],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => 'Software Testing Cost Calculator',
    'next' => NULL,
  ],

  'step21' =>   [
    'qss' =>     [
      'q-step21-0' => [
        'q' => 'How many hours were spent on development?',
        'a' => [
          'a-step21-0-0' => ['text' => '150 - 300 hrs',	'icon' => 'svg-time1', 'calc' => '12,24' ],
          'a-step21-0-1' => ['text' => '300 - 700 hrs',	'icon' => 'svg-time2', 'calc' => '42,98' ],
          'a-step21-0-2' => ['text' => '700+ hrs',			'icon' => 'svg-time3', 'calc' => '140,300' ],
          'a-step21-0-3' => ['text' => 'I don\'t know',	'icon' => 'svg-hz',		 'calc' => '12,24' ],
        ],
				'default' => 'a-step21-0-3',
        'type' => 'radio',
      ],
      'q-step21-1' => [
        'q' => 'How many screens does your project have?',
        'a' =>         [
          'a-step21-1-0' => [ 'text' => '0 - 10', 			'icon' => 'svg-screen1', 'calc' => '10' ],
          'a-step21-1-1' => [ 'text' => '10 - 25', 			'icon' => 'svg-screen2', 'calc' => '15' ],
          'a-step21-1-2' => [ 'text' => '25+', 					'icon' => 'svg-screen3', 'calc' => '20' ],
          'a-step21-1-3' => [ 'text' => 'I don\'t know','icon' => 'svg-hz', 		 'calc' => '10' ],
        ],
				'default' => 'a-step21-1-3',
        'type' => 'radio',
      ],
      'q-step21-2' => [
        'q' => 'What platforms need to be tested?',
        'a' =>         [
          'a-step21-2-0' => [ 'text' => 'Android', 			'icon' => 'svg-android', 'calc' => '1' ],
          'a-step21-2-1' => [ 'text' => 'iOS', 		 			'icon' => 'svg-ios', 		 'calc' => '1' ],
          'a-step21-2-2' => [ 'text' => 'Windows Phone','icon' => 'svg-winphon', 'calc' => '1' ],
          'a-step21-2-3' => [ 'text' => 'ALL', 					'icon' => 'svg-all', 		 'calc' => '3' ],
        ],
        'type' => 'checkbox',
      ],
      'q-step21-3' => [
        'q' => 'How many devices need to be tested?',
        'type' => 'number',
      ],
    ],
    'title' => '[rw_svg svg="mobile"] Can you tell us more about your mobile app?',
    'next' => 'step3',
  ],

  'step22' =>   [
    'qss' =>     [
      'q-step22-0' => [
        'q' => 'How many hours were spent on development?',
        'a' =>         [
          'a-step22-0-0' => [ 'text' => '150 - 300 hrs', 'icon' => 'svg-time1', 'calc' => '12,24' ],
          'a-step22-0-1' => [ 'text' => '300 - 700 hrs', 'icon' => 'svg-time2', 'calc' => '42,98' ],
          'a-step22-0-2' => [ 'text' => '700+ hrs', 		 'icon' => 'svg-time3', 'calc' => '140,300' ],
          'a-step22-0-3' => [ 'text' => 'I don\'t know', 'icon' => 'svg-hz',    'calc' => '12,24' ],
        ],
        'type' => 'radio',
      ],
      'q-step22-1' => [
        'q' => 'How many screens does your project have?',
        'a' =>         [
          'a-step22-1-0' => [ 'text' => '0 - 10', 			'icon' => 'svg-screen1', 'calc' => '10' ],
          'a-step22-1-1' => [ 'text' => '10 - 25',			'icon' => 'svg-screen2', 'calc' => '15' ],
          'a-step22-1-2' => [ 'text' => '25+', 					'icon' => 'svg-screen3', 'calc' => '20' ],
          'a-step22-1-3' => [ 'text' => 'I don\'t know','icon' => 'svg-hz', 		 'calc' => '10' ],
        ],
        'type' => 'radio',
      ],
      'q-step22-2' => [
        'q' => 'What operating systems need to be tested?',
        'a' =>         [
          'a-step22-2-0' => [ 'text' => 'Windows','icon' => 'svg-winphon', 'calc' => '1' ],
          'a-step22-2-1' => [ 'text' => 'macOS', 	'icon' => 'svg-ios', 		 'calc' => '1' ],
          'a-step22-2-2' => [ 'text' => 'Linux', 	'icon' => 'svg-linux', 	 'calc' => '1' ],
          'a-step22-2-3' => [ 'text' => 'ALL', 		'icon' => 'svg-all', 		 'calc' => '3' ],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => '[rw_svg svg="desktop"] Can you tell us more about your desktop app?',
    'next' => 'step3',
  ],

  'step23' =>   [
    'qss' =>     [
      'q-step23-0' => [
        'q' => 'How many hours were spent on development?',
        'a' =>  [
          'a-step23-0-0' => [ 'text' => '150 - 300 hrs', 'icon' => 'svg-time1', 'calc' => '12,24' ],
          'a-step23-0-1' => [ 'text' => '300 - 700 hrs', 'icon' => 'svg-time2', 'calc' => '42,98' ],
          'a-step23-0-2' => [ 'text' => '700+ hrs', 		 'icon' => 'svg-time3', 'calc' => '140,300' ],
          'a-step23-0-3' => [ 'text' => 'I don\'t know', 'icon' => 'svg-hz', 	 'calc' => '12,24' ],
        ],
        'type' => 'radio',
      ],
      'q-step23-1' => [
        'q' => 'How many pages does your project have?',
        'a' => [
          'a-step23-1-0' => [ 'text' => '0 - 300', 				'icon' => 'svg-screen1', 'calc' => '10' ],
          'a-step23-1-1' => [ 'text' => '300 - 2 000', 		'icon' => 'svg-screen2', 'calc' => '15' ],
          'a-step23-1-2' => [ 'text' => '2 000 - 10 000', 'icon' => 'svg-screen3', 'calc' => '20' ],
          'a-step23-1-3' => [ 'text' => 'I don\'t know', 	'icon' => 'svg-hz', 		 'calc' => '10' ],
        ],
        'type' => 'radio',
      ],
      'q-step23-2' => [
        'q' => 'What view resolutions does your project have?',
        'a' => [
          'a-step23-2-0' => [ 'text' => 'Mobile',  'icon' => 'svg-mobile', 'trigger_q' => 'web_m' ],
          'a-step23-2-1' => [ 'text' => 'Desktop', 'icon' => 'svg-desktop','trigger_q' => 'web_d' ],
        ],
        'type' => 'checkbox',
      ],
      'q-step23-3' => [
        'q' => 'How many mobile devices need to be tested?',
        'type' => 'number',
        'open_hook' => 'web_m',
      ],
      'q-step23-4' => [
        'q' => 'What mobile browsers need to be tested?',
        'a' => [
          'a-step23-4-0' => ['text' => 'Chrome','icon' => 'svg-chrome' ],
          'a-step23-4-1' => ['text' => 'Safari','icon' => 'svg-safari' ],
          'a-step23-4-2' => ['text' => 'Other', 'icon' => 'svg-other' ],
        ],
        'type' => 'checkbox',
        'open_hook' => 'web_m',
      ],
      'q-step23-5' => [
        'q' => 'How many desktop devices need to be tested?',
        'type' => 'number',
        'open_hook' => 'web_d',
      ],
      'q-step23-6' => [
        'q' => 'What desktop browsers need to be tested?',
        'a' =>         [
          'a-step23-6-0' => ['text' => 'Chrome',					'icon' => 'svg-chrome' ],
          'a-step23-6-1' => ['text' => 'Safari',					'icon' => 'svg-safari' ],
          'a-step23-6-2' => ['text' => 'Edge',						'icon' => 'svg-edge' ],
          'a-step23-6-3' => ['text' => 'Mozilla Firefox',	'icon' => 'svg-mozila' ],
          'a-step23-6-4' => ['text' => 'Opera',						'icon' => 'svg-opera' ],
          'a-step23-6-5' => ['text' => 'Other',						'icon' => 'svg-other' ],
        ],
        'type' => 'checkbox',
        'open_hook' => 'web_d',
      ],
    ],
    'title' => '[rw_svg svg="web"] Can you tell us more about the web part?',
    'next' => 'step3',
  ],
  'step24' => [
    'qss' => [
      'q-step24-0' => [
        'q' => 'How many hours were spent on development?',
        'a' =>         [
          'a-step24-0-0' => ['text' => '150 - 300 hrs',	'icon' => 'svg-time1', 'calc' => '12,24' ],
          'a-step24-0-1' => ['text' => '300 - 700 hrs',	'icon' => 'svg-time2', 'calc' => '42,98' ],
          'a-step24-0-2' => ['text' => '700 - 1 500',		'icon' => 'svg-time3', 'calc' => '140,300' ],
          'a-step24-0-3' => ['text' => 'I don\'t know',	'icon' => 'svg-hz',		 'calc' => '12,24' ],
        ],
        'type' => 'radio',
      ],
      'q-step24-1' => [
        'q' => 'What kinds of additional testing do you want us to perform?',
        'a' =>         [
          'a-step24-1-0' => ['text' => 'Functional testing',			'icon' => 'svg-func',  'calc' => '0.5' ],
          'a-step24-1-1' => ['text' => 'Security testing',				'icon' => 'svg-secur', 'calc' => '0.4' ],
          'a-step24-1-2' => ['text' => 'Automation load testing',	'icon' => 'svg-auto',  'calc' => '0.25' ],
          'a-step24-1-3' => ['text' => 'None',										'icon' => 'svg-hz',	   'calc' => '0' ],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => '[rw_svg svg="backend"] Can you tell us more about your backend?',
    'next' => 'step5',
  ],
  'step3' =>   [
    'qss' =>     [
      'q-step3-0' => [
        'q' => 'What basic features do you want to get tested?',
        'a' =>         [
					'a-step3-0-0' => ['text' => 'Accepting Payments',						'icon' => 'svg-accpay', 'calc' => '3' ],
          'a-step3-0-1' => ['text' => 'Comments/ Rating',							'icon' => 'svg-rating', 'calc' => '1' ],
          'a-step3-0-2' => ['text' => 'Installation/ Uninstallation', 'icon' => 'svg-uninst', 'calc' => '1' ],
          'a-step3-0-3' => ['text' => 'Search',											  'icon' => 'svg-search', 'calc' => '0.5' ],
          'a-step3-0-4' => ['text' => 'User Profile',								  'icon' => 'svg-profil', 'calc' => '0.5' ],
          'a-step3-0-5' => ['text' => 'Mobile Number Login',					'icon' => 'svg-mobnum', 'calc' => '0.25' ],
          'a-step3-0-6' => ['text' => 'Social Sharing',							  'icon' => 'svg-share',  'calc' => '0.5' ],
          'a-step3-0-7' => ['text' => 'Email Login',									'icon' => 'svg-email',  'calc' => '0.25' ],
          'a-step3-0-8' => ['text' => 'Shopping cart',								'icon' => 'svg-cart',	  'calc' => '2' ],
          'a-step3-0-9' => ['text' => 'Filters',											'icon' => 'svg-filter', 'calc' => '1' ],
          'a-step3-0-10' => ['text' => 'Creating Account',						'icon' => 'svg-create', 'calc' => '0.5' ],
          'a-step3-0-11' => ['text' => 'Social Login',								'icon' => 'svg-soclog', 'calc' => '0.5' ],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => '',
    'next' => 'step4',
  ],
  'step4' =>   [
    'qss' =>     [
      'q-step4-0' => [
        'q' => 'What additional features would you like to test?',
        'a' =>         [
          'a-step4-0-0' => ['text' => 'Maps',									'icon' => 'svg-maps',	  'calc' => '0.5'],
          'a-step4-0-1' => ['text' => 'Push notifications',		'icon' => 'svg-push',	  'calc' => '0.5'],
          'a-step4-0-2' => ['text' => 'Menu/Orders',					'icon' => 'svg-menu',	  'calc' => '2'],
          'a-step4-0-3' => ['text' => 'Geo Location',					'icon' => 'svg-geo',	  'calc' => '0.25'],
          'a-step4-0-4' => ['text' => 'Photos/Videos',				'icon' => 'svg-media',  'calc' => '0.5'],
          'a-step4-0-5' => ['text' => 'Events Registration', 	'icon' => 'svg-event',  'calc' => '1'],
          'a-step4-0-6' => ['text' => 'Reports',							'icon' => 'svg-report', 'calc' => '3'],
          'a-step4-0-7' => ['text' => 'Messaging',						'icon' => 'svg-msg',	  'calc' => '1'],
          'a-step4-0-8' => ['text' => 'Calendar',							'icon' => 'svg-calend', 'calc' => '0.5'],
          'a-step4-0-9' => ['text' => 'Upload/Download Files','icon' => 'svg-upload', 'calc' => '0.5'],
          'a-step4-0-10' => ['text' => 'Synchronization',			'icon' => 'svg-sync',	  'calc' => '0.5'],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => '',
    'next' => 'step5',
  ],
  'step5' =>   [
    'qss' =>     [
      'q-step5-0' => [
        'q' => 'Which development cycle do you want to get tested?',
        'a' =>         [
          'a-step5-0-0' => ['text' => 'Dev/ Stage', 	'icon' => 'svg-stage'],
          'a-step5-0-1' => ['text' => 'Prod',			 	 	'icon' => 'svg-prod'],
          'a-step5-0-2' => ['text' => 'I don\'t know','icon' => 'svg-hz'],
        ],
        'type' => 'checkbox',
      ],
    ],
    'title' => '',
    'next' => NULL,
  ],
];
