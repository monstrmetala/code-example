<style type="text/css">
	.intro, .summary {
		position: relative;
		padding-left: 37px;
		width: 100%;
	}
	.row {
		padding-left: 37px;
		position: relative;
	}
	.intro__logo {
		position: relative;
		z-index: 5;
		padding-top: 55px;
		width: 172px;
	}
	.intro__logo a {
		display: inline-block;
		position: absolute;
		top: 55px;
	}
	.intro__logo img {
		display: block;
		width: 170px;
		height: auto;
	}
	.intro__banner {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		width: 100%;
		height: 100%;
	}
	.abs-img__banner {
		position: absolute;
		top: -10px;
		left: 217px;
		width: 505px;
	}
	.abs-img__oval {
		position: absolute;
		top: 0px;
		left: 505px;
		width: 215px;
	}
	.intro__title {
		background-color: #dd3333;
		font-weight: bold;
		color: #fff;
		margin-top: -15px;
		font-size: 45px;
		padding: 15px 30px;
		width: 470px;
	}

	.section-title {
		font-size: 30px;
		margin-top: 35px;
		font-weight: bold;
		text-align: justify;
	}
	.estimate {
		background-color: #eee;
		border-left: 12px solid #ed4649;
		margin-top: 15px;
		width: 625px;
		height: 320px;
		position: relative;
	}

	.estimate-box_hz {
		height: 100%;
		width: 80px;
		position: absolute;
		top: 0;
		right: -80px;
	}

	.decor-1 {
		position: absolute;
		top: 0;
		left: 0;
		width: 0px;
		height: 0px;
		border: 40px solid #fefefe;
		border-top-color: transparent;
		border-right-color: transparent;
	}
	.decor-2 {
		position: absolute;
		top: 80px;
		left: 0;
		width: 80px;
		height: 240px;
		background-color: #eee;
	}
  .tst-cost {
		position: absolute;
		width: 40%;
		top: 60px;
  }

	.tst-cost-price {
		left: 110px;
	}
	.tst-cost-time {
		left: 393;
	}

	.tst-cost__name {
		text-transform: uppercase;
		font-size: 19px;
	}
	.tst-cost__price {
		font-size: 44px;
		color: #ed4649;
		font-weight: bold;
		margin-top: 5px;
	}
	.tst-cost__min, .tst-cost__max {
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
	}
	.tst-cost__min {
		margin-top: 23px;
	}
	.tst-cost__max {
		margin-top: 0px;
	}
	.vr-line {
		position: absolute;
		top: 60px;
		width: 1px;
		height: 200px;
		background-color: #979797;
		left: 55%;
	}

	.summary {
		margin-top: 1px;
	}
	.table-title {
		width: 650px;
		background-color: #202020;
		color: #fff;
		border-top-left-radius: 20px;
		border-top-right-radius: 20px;
		font-size: 26px;
		font-weight: bold;
		padding-top: 25px;
		padding-left: 68px;
		padding-right: 1px;
		padding-bottom: 25px;
		margin-top: 15px;

		text-align: justify;
	}
	.table-title--light { background-color: #b2c9d7; color: #202020; }
	.table-title--middle {
		border-top-left-radius: 0;
		border-top-right-radius: 0;
		border-radius: 0px;
	}
	.table-title--bottom {
		margin-top: 0px;
		border-top-left-radius: none;
		border-top-right-radius: none;
		border-bottom-left-radius: 20px;
		border-bottom-right-radius: 20px;
		padding: 20px 0 0;
		width: 720px;
	}
	tr {
		font-size: 19px;
		background-color: #f6f6f6;
	}

	tr.total {		background-color: #202020;	}
	.light tr.total {		background-color: #b2c9d7;	}
	td {
		font-weight: bold;
		font-size: 22px;
		padding: 18px 0;
		border-bottom: 2pt solid #fff;
		color: #202020;
	}

	.td--1 { font-size: 20px; width: 274px;padding-left: 20px;}
	.td--2 { width: 130px; }
	.td--3 { width: 135px; color: #949ca8; }
	.td--4 { width: 140px; color: #949ca8; }
	.td--234 { width: 405px; color: #949ca8; text-align: center;}

	.font-biger td.td--1 { font-size: 22px; }
	tr.th {
		background-color: #202020;
		color: #fff;
		font-size: 18px
	}
	
	tr.th td {
		padding: 15px 0;
		font-weight: normal;
		font-size: 18px;
		color: #fff;
	}
	tr.th td.td--1 { width: 180px;padding-left: 65px; }

	tr.total td.td--1 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--2 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--3 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--4 { color: #fff; border-bottom: none; padding: 20px 0 0;}

	tr.total2 { background-color: #fff; border-bottom: none; }

	td.td--end { padding: 0; background: none; border-bottom: none; }
	.td--end div {
		width: 100%;
		border-bottom-left-radius: 20px;
		border-bottom-right-radius: 20px;
		padding: 20px 0 0;
		width: 720px;
		background-color: #202020;
		margin-top: -1px; 
	}

	.light .td--end div { background-color: #b2c9d7; }

	.light tr.total td.td--1 { color: #202020; }
	.light tr.total td.td--2 { color: #202020; }
	.light tr.total td.td--3 { color: #202020; }
	.light tr.total td.td--4 { color: #202020; }
	.mt30 {margin-top: 90px; }

	.middle-text {
		position: relative;
		width: 760px;
		width: 720px;
		margin-top: 10px;
		padding-top: 40px;
		padding-bottom: 40px;
		padding-right: 40px;
		left: 0;
	}
	.middle-text p { 
		font-size: 20px;
		line-height: 1.6;
		position: relative;
		width: 700px;
		display: block;
	}
	.middle-text div {
		display: block;
		position: relative;
		width: 100%;
		text-align: right;
		font-size: 20px;
		font-weight: bold;
		width: 720px;
	}
	.book-box {
		text-align: center;
		position: relative;
		width: 200px;
		height: 180px;
	}
	.book div {
		width: 200px;
		height: 40px;
		background-color: #ed4649;
		color: #fff;
		line-height: 3;
		display: block;
		position: absolute;
		top: 0;
		left: 270px;
		padding-top: 20px;
		font-size: 18px;
		text-decoration: none;
	}
	.book {
		position: absolute;
		top: 0;
		left: 0;
		margin-top: 60px;
	}

	.cicle-d { font-size: 16px; color: #949ca8; position: relative; width: 200px; right: 250px;}
	.cicle-p { font-size: 16px; color: #949ca8; position: relative; width: 200px; right: 220px;}
	.cicle-b { font-size: 16px; color: #949ca8; position: relative; width: 200px; right: 320px;}
	
	tr.thf { background-color: #b2c9d7; }
	tr.thf td {	color: #000; }
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header></page_header>
    <page_footer></page_footer>
    <div class="intro">
			<div class="intro__logo">
				<a href="https://qawerk.com/" title="Title" ><img src="<?php echo PDF_IMGS . 'logo.png'; ?>" alt=""></a>
			</div>
			<div class="intro__banner">
				<img src="<?php echo PDF_IMGS . 'shutterstock.png'; ?>" alt="" class="abs-img abs-img__banner">
				<img src="<?php echo PDF_IMGS . 'oval.png'; ?>" alt="" class="abs-img abs-img__oval">
			</div>
      <div class="intro__title"><span>My Software Testing</span></div>

			<div class="round-estimate">
				<h2 class="section-title">Rough Estimate</h2>
				<div class="estimate">
					
						<div class="tst-cost tst-cost-price">
							<h2 class="tst-cost__name">TESTING COST</h2>
							<span class="tst-cost__price">$ <?php echo $info['price_mid']; ?></span>
							<p class="tst-cost__min">Max $<?php echo $info['price_max']; ?></p>
							<p class="tst-cost__max">Min $<?php echo $info['price_min']; ?></p>
						</div>

						<div class="vr-line"></div>

						<div class="tst-cost tst-cost-time">
							<h2 class="tst-cost__name">Summary Time</h2>
							<span class="tst-cost__price"><?php echo $info['time_middle']; ?> h</span>
							<p class="tst-cost__min">Max <?php echo $info['time_max']; ?> h</p>
							<p class="tst-cost__max">Min <?php echo $info['time_min']; ?> h</p>
						</div>

					<div class="estimate-box_hz">
						<div class="decor-1"></div>
						<div class="decor-2"></div>
					</div>
				</div>
	
			</div>

    </div>

    <div class="summary">
			<h2 class="section-title">Summary</h2>
			
			<div class="table-title">
				<span>Software Testing Time</span>
				<?php echo $info['cycle'] ? $info['cycle'] : ''; ?>
			</div>
			<table cellspacing="0" class="font-biger">
				<tr class="th">
					<td class="td--1">Section</td>
					<td class="td--2">Time, h</td>
					<td class="td--3">Min, h</td>
					<td class="td--4">Max, h</td>
				</tr>

				<?php if(isset($info['software']['mob'])): ?>
				<tr>
					<td class="td--1">Mobile</td>
					<td class="td--2"><?php echo $info['software']['mob'][2]; ?></td>
					<td class="td--3"><?php echo $info['software']['mob'][0]; ?></td>
					<td class="td--4"><?php echo $info['software']['mob'][1]; ?></td>
				</tr>
				<?php endif; ?>

				<?php if(isset($info['software']['desk'])): ?>
					<tr>
						<td class="td--1">Desktop</td>
						<td class="td--2"><?php echo $info['software']['desk'][2]; ?></td>
						<td class="td--3"><?php echo $info['software']['desk'][0]; ?></td>
						<td class="td--4"><?php echo $info['software']['desk'][1]; ?></td>
					</tr>
				<?php endif; ?>

				<?php if(isset($info['software']['web'])): ?>
					<tr>
						<td class="td--1">Web</td>
						<td class="td--2"><?php echo $info['software']['web'][2]; ?></td>
						<td class="td--3"><?php echo $info['software']['web'][0]; ?></td>
						<td class="td--4"><?php echo $info['software']['web'][1]; ?></td>
					</tr>
				<?php endif; ?>

				<?php if(isset($info['software']['back'])): ?>
					<tr>
						<td class="td--1">Backend</td>
						<td class="td--2"><?php echo $info['software']['back'][2]; ?></td>
						<td class="td--3"><?php echo $info['software']['back'][0]; ?></td>
						<td class="td--4"><?php echo $info['software']['back'][1]; ?></td>
					</tr>
				<?php endif; ?>

			</table>
			
		</div>

</page>


<page pageset="old">
	<div class="row">

		<?php if(isset($info['mob'])): ?>
			<div class="table-title table-title--middle">Mobile In Details</div>
			<table cellspacing="0">
				<tr>
					<td class="td--1">Basic product complexity</td>
					<td class="td--2"><?php echo $info['mob']['base']['mid']; ?></td>
					<td class="td--3"><?php echo $info['mob']['base'][0]; ?></td>
					<td class="td--4"><?php echo $info['mob']['base'][1]; ?></td>
				</tr>
				<tr>
					<td class="td--1">Screens</td>
					<td class="td--2"><?php echo $info['mob']['screens']; ?></td>
					<td class="td--3"><?php echo $info['mob']['screens']; ?></td>
					<td class="td--4"><?php echo $info['mob']['screens']; ?></td>
				</tr>
				
				<?php if(isset($info['mob']['devices'])): ?>
				<tr>
					<td class="td--1">Devices</td>
					<td class="td--2"><?php echo $info['mob']['devices'][2]; ?></td>
					<td class="td--3"><?php echo $info['mob']['devices'][0]; ?></td>
					<td class="td--4"><?php echo $info['mob']['devices'][1]; ?></td>
				</tr>
				<?php endif; ?>

			</table>
		<?php endif; ?>

		<?php if(isset($info['desktop'])): ?>
			<div class="table-title table-title--middle">Desktop In Details</div>
			<table cellspacing="0">
			<tr>
					<td class="td--1">Basic product complexity</td>
					<td class="td--2"><?php echo $info['desktop']['base']['mid']; ?></td>
					<td class="td--3"><?php echo $info['desktop']['base'][0]; ?></td>
					<td class="td--4"><?php echo $info['desktop']['base'][1]; ?></td>
				</tr>
				<tr>
					<td class="td--1">Screens</td>
					<td class="td--2"><?php echo $info['desktop']['screens']; ?></td>
					<td class="td--3"><?php echo $info['desktop']['screens']; ?></td>
					<td class="td--4"><?php echo $info['desktop']['screens']; ?></td>
				</tr>
				
				<?php if(isset($info['desktop']['os'])): ?>
				<tr>
					<td class="td--1">Operating systems</td>
					<td class="td--2"><?php echo $info['desktop']['os'][2]; ?></td>
					<td class="td--3"><?php echo $info['desktop']['os'][0]; ?></td>
					<td class="td--4"><?php echo $info['desktop']['os'][1]; ?></td>
				</tr>
				<?php endif; ?>

			</table>
		<?php endif; ?>

		<?php if(isset($info['web'])): ?>
			<div class="table-title table-title--middle">Web In Details</div>
			<table cellspacing="0">
				<tr>
					<td class="td--1">Basic product complexity</td>
					<td class="td--2"><?php echo $info['web']['base']['mid']; ?></td>
					<td class="td--3"><?php echo $info['web']['base'][0]; ?></td>
					<td class="td--4"><?php echo $info['web']['base'][1]; ?></td>
				</tr>
				
				<?php if(isset($info['web']['pages'])): ?>
				<tr>
					<td class="td--1">Pages</td>
					<td class="td--2"><?php echo $info['web']['pages']; ?></td>
					<td class="td--3"><?php echo $info['web']['pages']; ?></td>
					<td class="td--4"><?php echo $info['web']['pages']; ?></td>
				</tr>
				<?php endif; ?>
				
				<?php if(isset($info['web']['devices'])): ?>
				<tr>
					<td class="td--1">Devices</td>
					<td class="td--2"><?php echo $info['web']['devices'][2]; ?></td>
					<td class="td--3"><?php echo $info['web']['devices'][0]; ?></td>
					<td class="td--4"><?php echo $info['web']['devices'][1]; ?></td>
				</tr>
				<?php endif; ?>

				<?php if(isset($info['web']['browsers'])): ?>
				<tr>
					<td class="td--1">Browsers</td>
					<td class="td--2"><?php echo $info['web']['browsers'][2]; ?></td>
					<td class="td--3"><?php echo $info['web']['browsers'][0]; ?></td>
					<td class="td--4"><?php echo $info['web']['browsers'][1]; ?></td>
				</tr>
				<?php endif; ?>

			</table>
		<?php endif; ?>

		<?php if(isset($info['back'])): ?>
			<div class="table-title table-title--middle">Backend In Details</div>
			<table cellspacing="0">
			<tr>
					<td class="td--1">Basic product complexity</td>
					<td class="td--2"><?php echo $info['back']['base']['mid']; ?></td>
					<td class="td--3"><?php echo $info['back']['base'][0]; ?></td>
					<td class="td--4"><?php echo $info['back']['base'][1]; ?></td>
				</tr>

				<?php 
				if (isset($info['back']['kinds']))
				foreach ($info['back']['kinds'] as $kind): ?>
					<tr>
						<td class="td--1"><?php echo $kind['name']; ?></td>
						<td class="td--2"><?php echo $kind['time'][2]; ?></td>
						<td class="td--3"><?php echo $kind['time'][0]; ?></td>
						<td class="td--4"><?php echo $kind['time'][1]; ?></td>
					</tr>
				<?php endforeach; ?>

			</table>
		<?php endif; ?>
	</div>

	<div class="row">
		<?php if(isset($info['features'])): ?>
			<div class="table-title table-title--light mt30">Features</div>
			<table cellspacing="0" class="light">
				<tr class="th thf">
					<td class="td--1">Section</td>
					<td class="td--234">Time, h</td>
				</tr>
				<?php foreach ($info['features'] as $f): ?>
					<tr>
						<td class="td--1"><?php echo $f['name']; ?></td>
						<td class="td--234"><?php echo $f['time']; ?>h</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>
	</div>

</page>

<page pageset="old">
	<div class="row">
		<div class="middle-text">
			<p>* This is the rough estimate for informative purposes. Though the estimate is based on our previous projects and experience, it's not 100% accurate. Let's get in touch so that you could share your software's special story and we could offer you the best timing and budget fit.</p>
			<div><?php echo date('j F Y')?></div>
		</div>
	</div>
	
	<?php if (isset($info['meet_url']) && !empty($info['meet_url'])):	?>
		<div class="row">
			<p class="book-box">
				<a href="<?php echo $info['meet_url']; ?>" title="Book meeting" class="book">
					<div>Book meeting</div>
				</a>
			</p>
		</div>
	<?php endif; ?>



</page>

