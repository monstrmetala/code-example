<style type="text/css">
	.intro, .summary {
		position: relative;
		padding-left: 37px;
		width: 100%;
	}
	.row {
		padding-left: 37px;
		position: relative;
	}
	.intro__logo {
		position: relative;
		z-index: 5;
		padding-top: 55px;
		width: 172px;
	}
	.intro__logo a {
		display: inline-block;
		position: absolute;
		top: 55px;
	}
	.intro__logo img {
		display: block;
		width: 170px;
		height: auto;
	}
	.intro__banner {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		width: 100%;
		height: 100%;
	}
	.abs-img__banner {
		position: absolute;
		top: -10px;
		left: 217px;
		width: 505px;
	}
	.abs-img__oval {
		position: absolute;
		top: 0px;
		left: 505px;
		width: 215px;
	}
	.intro__title {
		background-color: #dd3333;
		font-weight: bold;
		color: #fff;
		margin-top: -15px;
		font-size: 45px;
		padding: 15px 30px;
		width: 470px;
	}

	.section-title {
		font-size: 30px;
		margin-top: 35px;
		font-weight: bold;

	}
	.estimate {
		background-color: #eee;
		border-left: 12px solid #ed4649;
		margin-top: 15px;
		width: 625px;
		height: 320px;
		position: relative;
	}

	.estimate-box_hz {
		height: 100%;
		width: 80px;
		position: absolute;
		top: 0;
		right: -80px;
	}

	.decor-1 {
		position: absolute;
		top: 0;
		left: 0;
		width: 0px;
		height: 0px;
		border: 40px solid #fefefe;
		border-top-color: transparent;
		border-right-color: transparent;
	}
	.decor-2 {
		position: absolute;
		top: 80px;
		left: 0;
		width: 80px;
		height: 240px;
		background-color: #eee;
	}
  .tst-cost {
		position: absolute;
		width: 40%;
		top: 60px;
  }

	.tst-cost-price {
		left: 110px;
	}
	.tst-cost-time {
		left: 393;
	}

	.tst-cost__name {
		text-transform: uppercase;
		font-size: 19px;
	}
	.tst-cost__price {
		font-size: 44px;
		color: #ed4649;
		font-weight: bold;
		margin-top: 5px;
	}
	.tst-cost__min, .tst-cost__max {
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
	}
	.tst-cost__min {
		margin-top: 23px;
	}
	.tst-cost__max {
		margin-top: 0px;
	}
	.vr-line {
		position: absolute;
		top: 60px;
		width: 1px;
		height: 200px;
		background-color: #979797;
		left: 55%;
	}

	.summary {
		margin-top: 12px;
	}
	.table-title {
		width: 650px;
		background-color: #202020;
		color: #fff;
		border-top-left-radius: 20px;
		border-top-right-radius: 20px;
		/* overflow: hidden; */
		font-size: 26px;
		font-weight: bold;
		padding-top: 25px;
		padding-left: 68px;
		padding-right: 1px;
		padding-bottom: 25px;
		margin-top: 20px;
	}
	
	.table-title--light { background-color: #b2c9d7; color: #202020; }
	.table-title--middle {
		border-top-left-radius: 0;
		border-top-right-radius: 0;
		border-radius: 0px;
	}
	.table-title--bottom {
		margin-top: 0px;
		border-top-left-radius: none;
		border-top-right-radius: none;
		border-bottom-left-radius: 20px;
		border-bottom-right-radius: 20px;
		padding: 20px 0 0;
		width: 720px;
	}
	tr {
		font-size: 19px;
		background-color: #f6f6f6;
	}

	tr.total {		background-color: #202020;	}
	.light tr.total {		background-color: #b2c9d7;	}
	td {
		font-weight: bold;
		font-size: 22px;
		padding: 18px 0;
		border-bottom: 2pt solid #fff;
		color: #202020;
	}

	.td--1 { font-size: 20px; width: 274px;padding-left: 20px;}
	.td--2 { width: 130px; }
	.td--3 { width: 135px; color: #949ca8; }
	.td--4 { width: 140px; color: #949ca8; }

	.font-biger td.td--1 { font-size: 22px; }
	tr.th {
		background-color: #202020;
		color: #fff;
		font-size: 18px
	}
	tr.th td {
		padding: 15px 0;
		font-weight: normal;
		font-size: 18px;
		color: #fff;
	}
	tr.th td.td--1 { width: 180px;padding-left: 65px; }

	tr.total td.td--1 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--2 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--3 { color: #fff; border-bottom: none; padding: 20px 0 0;}
	tr.total td.td--4 { color: #fff; border-bottom: none; padding: 20px 0 0;}

	tr.total2 { background-color: #fff; border-bottom: none; }

	td.td--end { padding: 0; background: none; border-bottom: none; }
	.td--end div {
		width: 100%;
		border-bottom-left-radius: 20px;
		border-bottom-right-radius: 20px;
		padding: 20px 0 0;
		width: 720px;
		background-color: #202020;
		margin-top: -1px; 
	}

	.light .td--end div { background-color: #b2c9d7; }

	.light tr.total td.td--1 { color: #202020; }
	.light tr.total td.td--2 { color: #202020; }
	.light tr.total td.td--3 { color: #202020; }
	.light tr.total td.td--4 { color: #202020; }
	.mt30 {margin-top: 50px; }

	.middle-text {
		position: relative;
		width: 760px;
		width: 720px;
		margin-top: 10px;
		padding-top: 40px;
		padding-bottom: 40px;
		padding-right: 40px;
		left: 0;
		background-image: url(<?php echo PDF_IMGS . 'oval-solid_small250.png'; ?>);
		background-repeat: no-repeat;
		background-position: 550px 0px;
	}
	.middle-text p { 
		font-size: 20px;
		line-height: 1.6;
		position: relative;
		width: 700px;
		display: block;
	}
	.middle-text div {
		display: block;
		position: relative;
		width: 100%;
		text-align: right;
		font-size: 20px;
		font-weight: bold;
		width: 720px;
	}
	.book-box {
		text-align: center;
		position: relative;
	}
	.book div {
		width: 200px;
		height: 40px;
		background-color: #ed4649;
		color: #fff;
		line-height: 3;
		display: block;
		position: absolute;
		top: 0;
		left: 270px;
		padding-top: 20px;
		font-size: 18px;
		text-decoration: none;
	}
	.book {
		position: absolute;
		top: 0;
		left: 0;
		margin-top: 60px;
	}

	
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header></page_header>
    <page_footer></page_footer>
    <div class="intro">
			<div class="intro__logo">
				<a href="https://qawerk.com/" title="Title" ><img src="<?php echo PDF_IMGS . 'logo.png'; ?>" alt=""></a>
			</div>
			<div class="intro__banner">
				<img src="<?php echo PDF_IMGS . 'shutterstock.png'; ?>" alt="" class="abs-img abs-img__banner">
				<img src="<?php echo PDF_IMGS . 'oval.png'; ?>" alt="" class="abs-img abs-img__oval">
			</div>
      <div class="intro__title"><span>My Software Testing</span></div>

			<div class="round-estimate">
				<h2 class="section-title">Rough Estimate</h2>
				<div class="estimate">
					
						<div class="tst-cost tst-cost-price">
							<h2 class="tst-cost__name">TESTING COST</h2>
							<span class="tst-cost__price">$ 9450</span>
							<p class="tst-cost__min">Max $7577</p>
							<p class="tst-cost__max">Min $11322</p>
						</div>
			
						<div class="vr-line"></div>

						<div class="tst-cost tst-cost-time">
							<h2 class="tst-cost__name">Summary Time</h2>
							<span class="tst-cost__price">282.2 h</span>
							<p class="tst-cost__min">Min 226.3 h</p>
							<p class="tst-cost__max">Max 338.2 h</p>
						</div>

					<div class="estimate-box_hz">
						<div class="decor-1"></div>
						<div class="decor-2"></div>
					</div>
				</div>
	
			</div>

    </div>

    <div class="summary">
			<h2 class="section-title">Summary</h2>
			
			<div class="table-title">Software Testing Time</div>
			<table cellspacing="0" class="font-biger">
				<tr class="th">
					<td class="td--1">Section</td>
					<td class="td--2">Time, h</td>
					<td class="td--3">Min, h</td>
					<td class="td--4">Max, h</td>
				</tr>
				<tr>
					<td class="td--1">Mobile</td>
					<td class="td--2">62.3</td>
					<td class="td--3">62.9</td>
					<td class="td--4">62.9</td>
				</tr>
				<tr>
					<td class="td--1">Desktop</td>
					<td class="td--2">62.3</td>
					<td class="td--3">62.9</td>
					<td class="td--4">62.9</td>
				</tr>
				<tr>
					<td class="td--1">Web</td>
					<td class="td--2">62.3</td>
					<td class="td--3">62.9</td>
					<td class="td--4">62.9</td>
				</tr>
				<tr>
					<td class="td--1">Backend</td>
					<td class="td--2">62.3</td>
					<td class="td--3">62.9</td>
					<td class="td--4">62.9</td>
				</tr>
			</table>
			
		</div>

</page>


<page pageset="old">
	<div class="row">
		<div class="table-title table-title--middle">Mobile In Details</div>
		<table cellspacing="0">
			<tr>
				<td class="td--1">Mobile</td>
				<td class="td--2">62.3</td>
				<td class="td--3">62.9</td>
				<td class="td--4">62.9</td>
			</tr>
			<tr class="total">
				<td class="td--1">Total</td>
				<td class="td--2">62.3</td>
				<td class="td--3">62.9</td>
				<td class="td--4">62.9</td>
			</tr>
			<tr class="total2">
				<td class="td--end" colspan="4"><div></div></td>
			</tr>
		</table>

		<div class="table-title table-title--light mt30">Features</div>
		<table cellspacing="0" class="light">
			<tr>
				<td class="td--1">Accepting Payments</td>
				<td class="td--2">62.3</td>
				<td class="td--3">62.9</td>
				<td class="td--4">62.9</td>
			</tr>
			<tr>
				<td class="td--1">Comments/ Rating</td>
				<td class="td--2">62.3</td>
				<td class="td--3">62.9</td>
				<td class="td--4">62.9</td>
			</tr>
			<tr class="total">
				<td class="td--1">Total</td>
				<td class="td--2">62.3</td>
				<td class="td--3">62.9</td>
				<td class="td--4">62.9</td>
			</tr>
			<tr class="total2">
				<td class="td--end" colspan="4"><div></div></td>
			</tr>
		</table>
		
		<div class="middle-text">
			<p>* This is the rough estimate for informative purposes. Though the estimate is based on our previous projects and experience, it's not 100% accurate. Let's get in touch so that you could share your software's special story and we could offer you the best timing and budget fit.</p>
			<div>19 February 2021</div>
		</div>

		<p class="book-box">
			<a href="https://www.google.com.ua/?hl=ru" title="Book meeting" class="book">
				<div>Book meeting</div>
			</a>
		</p>
		
	</div>
</page>

<?php /*/ ?>
<page pageset="old">
    <bookmark title="Balises classiques" level="2" ></bookmark>
    <div class="note">
        La liste des balises HTML utilisables est la suivante :<br>
    </div>
    <br>
    <ul class="main">
        <li>&lt;a&gt; : Ceci est un lien vers <a href="http://html2pdf.fr">le site de Html2Pdf</a></li>
        <li>&lt;b&gt;, &lt;strong&gt; : Ecrire en <b>gras</b>.</li>
        <li>&lt;big&gt; : Ecrire plus <big>gros</big>.</li>
        <li>&lt;br&gt; : Permet d'aller à la ligne</li>
        <li>&lt;cite&gt; : <cite>Ceci est une citation</cite></li>
        <li>&lt;code&gt;, &lt;pre&gt;</li>
        <li>&lt;div&gt; :&nbsp;<div style="border: solid 1px #AADDAA; background: #DDFFDD; text-align: center; width: 50mm">exemple de DIV</div></li>
        <li>&lt;em&gt;, &lt;i&gt;, &lt;samp&gt; : Ecrire en <em>italique</em>.</li>
        <li>&lt;font&gt;, &lt;span&gt; : <font style="color: #000066; font-family: times">Exemple d'utilisation</font></li>
        <li>&lt;h1&gt;, &lt;h2&gt;, &lt;h3&gt;, &lt;h4&gt;, &lt;h5&gt;, &lt;h6&gt;</li>
        <li>&lt;hr&gt; : barre horizontale</li>
        <li>&lt;img&gt; : <img src="https://darkness.zp.ua/pdf/liba/examples/res/tcpdf_logo.jpg" style="width: 10mm"></li>
        <li>&lt;p&gt; : Ecrire dans un paragraphe</li>
        <li>&lt;s&gt; : Texte <s>barré</s></li>
        <li>&lt;small&gt; : Ecrire plus <small>petit</small>.</li>
        <li>&lt;style&gt;</li>
        <li>&lt;sup&gt; : Exemple<sup>haut</sup>.</li>
        <li>&lt;sub&gt; : Exemple<sub>bas</sub>.</li>
        <li>&lt;u&gt; : Texte <u>souligné</u></li>
        <li>&lt;table&gt;, &lt;td&gt;, &lt;th&gt;, &lt;tr&gt;, &lt;thead&gt;, &lt;tbody&gt;, &lt;tfoot&gt;, &lt;col&gt;, &lt;colgroup&gt; </li>
        <li>&lt;ol&gt;, &lt;ul&gt;, &lt;li&gt;</li>
        <li>&lt;form&gt;, &lt;input&gt;, &lt;textarea&gt;, &lt;select&gt;, &lt;option&gt;</li>
        <li>&lt;fieldset&gt;, &lt;legend&gt;</li>
        <li>&lt;del&gt;, &lt;ins&gt;</li>
        <li>&lt;draw&gt;, &lt;line&gt;, &lt;rect&gt;, &lt;circle&gt;, &lt;ellipse&gt;, &lt;polygone&gt;, &lt;polyline&gt;, &lt;path&gt;</li>
    </ul>
    <bookmark title="Balises spécifiques" level="2" ></bookmark>
    <div class="note">
        Les balises spécifiques suivantes ont été ajoutées :<br>
    </div>
    <br>
    <ul class="main" >
        <li>&lt;page&gt;</li>
        <li>&lt;page_header&gt;</li>
        <li>&lt;page_footer&gt;</li>
        <li>&lt;end_last_page&gt;</li>
        <li>&lt;nobreak&gt;</li>
        <li>&lt;barcode&gt;</li>
        <li>&lt;bookmark&gt;</li>
        <li>&lt;qrcode&gt;</li>
    </ul>
</page>

<page pageset="old">
    <bookmark title="Styles CSS" level="1" ></bookmark>
    <div class="note">
        La liste des styles CSS utilisables est la suivante :<br>
    </div>
    <br>
    <table style="width: 100%">
        <tr style="vertical-align: top">
            <td style="width: 50%">
                <ul class="main">
                    <li>color</li>
                    <li>font-family</li>
                    <li>font-weight</li>
                    <li>font-style</li>
                    <li>font-size</li>
                    <li>text-decoration</li>
                    <li>text-indent</li>
                    <li>text-align</li>
                    <li>text-transform</li>
                    <li>vertical-align</li>
                    <li>width</li>
                    <li>height</li>
                    <li>line-height</li>
                    <li>padding</li>
                    <li>padding-top</li>
                    <li>padding-right</li>
                    <li>padding-bottom</li>
                    <li>padding-left</li>
                    <li>margin</li>
                    <li>margin-top</li>
                    <li>margin-right</li>
                    <li>margin-bottom</li>
                    <li>margin-left</li>
                    <li>position</li>
                    <li>top</li>
                    <li>bottom</li>
                    <li>left</li>
                    <li>right</li>
                    <li>float</li>
                    <li>rotate</li>
                    <li>background</li>
                    <li>background-color</li>
                    <li>background-image</li>
                    <li>background-position</li>
                    <li>background-repeat</li>
                </ul>
            </td>
            <td style="width: 50%">
                <ul class="main">
                    <li>border</li>
                    <li>border-style</li>
                    <li>border-color</li>
                    <li>border-width</li>
                    <li>border-collapse</li>
                    <li>border-top</li>
                    <li>border-top-style</li>
                    <li>border-top-color</li>
                    <li>border-top-width</li>
                    <li>border-right</li>
                    <li>border-right-style</li>
                    <li>border-right-color</li>
                    <li>border-right-width</li>
                    <li>border-bottom</li>
                    <li>border-bottom-style</li>
                    <li>border-bottom-color</li>
                    <li>border-bottom-width</li>
                    <li>border-left</li>
                    <li>border-left-style</li>
                    <li>border-left-color</li>
                    <li>border-left-width</li>
                    <li>border-radius</li>
                    <li>border-top-left-radius</li>
                    <li>border-top-right-radius</li>
                    <li>border-bottom-left-radius</li>
                    <li>border-bottom-right-radius</li>
                    <li>list-style</li>
                    <li>list-style-type</li>
                    <li>list-style-image</li>
                </ul>
            </td>
        </tr>
    </table>
</page>

<page pageset="old">
    <bookmark title="Propriétés" level="1" ></bookmark>
    <div class="note">
        La liste des propriétés utilisables est la suivante :<br>
    </div>
    <br>
    <table style="width: 100%">
        <tr style="vertical-align: top">
            <td style="width: 50%">
                <ul class="main">
                    <li>cellpadding</li>
                    <li>cellspacing</li>
                    <li>colspan</li>
                    <li>rowspan</li>
                    <li>width</li>
                    <li>height</li>
                </ul>
            </td>
            <td style="width: 50%">
                <ul class="main">
                    <li>align</li>
                    <li>valign</li>
                    <li>bgcolor</li>
                    <li>bordercolor</li>
                    <li>border</li>
                    <li>type</li>
                    <li>value</li>
                </ul>
            </td>
        </tr>
    </table>
    <bookmark title="Limitations" level="0" ></bookmark>
    <div class="note">
        Cette librairie comporte des limitations :<br>
    </div>
    <br>
    <ul class="main">
        <li>Les float ne sont gérés que pour la balise IMG.</li>
        <li>Elle ne permet généralement pas la conversion directe d'une page HTML en PDF, ni la conversion du résultat d'un WYSIWYG en PDF.</li>
        <li>Cette librairie est là pour faciliter la génération de documents PDF, pas pour convertir n'importe quelle page HTML.</li>
        <li>Les formulaires ne marchent pas avec tous les viewers PDFs...</li>
        <li>Lisez bien la documentation : <a href="https://github.com/spipu/html2pdf/blob/master/doc/README.md">https://github.com/spipu/html2pdf/blob/master/doc/README.md</a>.</li>
    </ul>
</page>
<?php /*/ ?>
