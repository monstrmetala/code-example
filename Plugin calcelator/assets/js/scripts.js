document.addEventListener('DOMContentLoaded', () => {
  const ajaxurl = '/wp-admin/admin-ajax.php';
  class Calculator {
    
    constructor(calc) {
      this.calc = calc;
      this.form = calc.querySelector('[data-rw-calculator-form]');
      if (!this.form) return;
      this.submitBox = calc.querySelector('[data-rw-submit]');
      if (!this.submitBox) return;



			// RANGE Handler
      calc.querySelectorAll('[data-rw-calculator-range]').forEach( box => {
				let rangeValue; 

				const tooltip = box.querySelector('span');
				box.querySelector('input').addEventListener('input', (e) => {
					const inp = e.target;
					rangeValue = parseInt(e.target.value);
					tooltip.innerText = rangeValue;
					
					const rangeValuePs = (rangeValue ) * 20; //* 25
					const rangeValuePx = (rangeValue -1 ) * 12; //* 12
					
					tooltip.style.left = `calc(${rangeValuePs}% - ${rangeValuePx}px)`;
				}); 
			});

      // Time To Test
      this.tttMin = document.querySelector('[data-ttt-min]');
      this.tttMax = document.querySelector('[data-ttt-max]');
      calc.querySelectorAll('input').forEach( input => { input.addEventListener('change', this.calcTTT.bind(this) ) });

			// Trigger Open - answer
			calc.querySelectorAll('[data-trigger-open]').forEach( input => { input.addEventListener('change', this.triggerOpenHandler.bind(this) ) });

			// Trigger Open - question
			calc.querySelectorAll('[data-quest-trigger-open]').forEach( quest => {
				
				const inputs = quest.querySelectorAll('input');
				inputs.forEach(  input => { 
					
					if (input.type === 'range') {

						input.addEventListener('input', () => {
							let targetId = quest.dataset.questTriggerOpen;
							let targetEl = this.calc.querySelector(`.quest-id__${targetId}`);
							if (targetEl) {

								if (input.value == '0' ) {
									targetEl.classList.remove( `quest-openby__${quest.dataset.questKey}` );
								} else {
									targetEl.classList.add( `quest-openby__${quest.dataset.questKey}` );
								}
							}
						});

					} else {

						input.addEventListener('change', (inp) => {
	
							let hasChecked = false;
							inputs.forEach(  input => { 
								if (input.checked) hasChecked = true;
							});
	
							let targetId = quest.dataset.questTriggerOpen;
							let targetEl;
							let triggerClaasName;

							if ( targetId === '_finish') {
								targetEl = this.submitBox;
								triggerClaasName = 'rqw-actions--show';
							} else {
								targetEl = this.calc.querySelector(`.quest-id__${targetId}`);
								triggerClaasName = `quest-openby__${quest.dataset.questKey}`;
							}

							if (targetEl) {
								if (hasChecked) {
									targetEl.classList.add( triggerClaasName );
								} else {
									targetEl.classList.remove( triggerClaasName );
								}
							}
	
						});
					}
				});



				//  q.addEventListener('change', q.triggerOpenHandler.bind(this) ) 
			});

			// ALL answer
			calc.querySelectorAll('[data-quest-has-all]').forEach( quest => {
				let inputAll = quest.querySelector('[data-answer-type="all"]');
				if (!inputAll) return;
				
				let otherInputs = quest.querySelectorAll('input:not([data-answer-type="all"])');

				inputAll.addEventListener('change', () => {
					if (inputAll.checked) {
						otherInputs.forEach( otherInput => {
							otherInput.checked = true;
						});
					}
				});

				/* If unchecked one of Other onput in pack */
				otherInputs.forEach( otherInput => {
					otherInput.addEventListener('change', () => {
						if (inputAll.checked && !otherInput.checked) {
							inputAll.checked = false;
						}
					});
				});
			});
    }
		
		triggerOpenHandler(event) {
			const triggerEl = event.target;
			const targetId = triggerEl.dataset.triggerOpen;

			const targetEl = this.calc.querySelector(`.quest-id__${targetId}`);
			if (targetEl) {
				targetEl.classList.toggle( `quest-openby__${triggerEl.value}` );
			}
		}
		
    // Time To Test - HANDLER
    calcTTT() {

      let serialize = jQuery(this.form).serializeArray();
      _this = this;

      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'cacl_calculation',
            form: serialize,
        },
      }).done(function( data ) {
          try {
            var answer = JSON.parse( data );
            if( answer.status == "ok" ){
              _this.tttMin.innerText = answer.min || 0;
              _this.tttMax.innerText = answer.max || 0;
            }
          } catch (error) {
          }
      });

    }
  }

  document.querySelectorAll('[data-rw-calculator]').forEach( calc => new Calculator(calc));
});

