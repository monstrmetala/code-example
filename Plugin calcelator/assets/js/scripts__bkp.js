document.addEventListener('DOMContentLoaded', () => {
  const ajaxurl = '/wp-admin/admin-ajax.php';
  class Calculator {
    
    constructor(calc) {
      this.calc = calc;
      this.form = calc.querySelector('[data-rw-calculator-form]');
      if (!this.form) return;

      this.inputTroggers = calc.querySelectorAll('[data-trigger-next]');
      this.submitBox = calc.querySelector('[data-rw-submit]');
      this.nextBox = calc.querySelector('[data-rw-next]');
      this.nextBtn = calc.querySelector('[data-rw-next-btn]');
      this.nextStep = false;
      this.showSubmit = false;
      this.tttMin = document.querySelector('[data-ttt-min]');
      this.tttMax = document.querySelector('[data-ttt-max]');
      this.ranges = calc.querySelectorAll('[data-rw-calculator-range]');

      this.pppTrigger = document.querySelector('[date-rw-calculator-popup-trigger]');
      this.ppp = document.querySelector('[date-rw-calculator-popup]');
      this.pppPlan = this.ppp.querySelector('[data-rwc-plan]');
      this.pppQuoteOptH = this.ppp.querySelector('[data-rwc-ppp-opt-h] b');
      this.pppQuoteOptD = this.ppp.querySelector('[data-rwc-ppp-opt-d] b');
      this.pppQuoteOptM = this.ppp.querySelector('[data-rwc-ppp-opt-m] b');
      this.pppQuotePesH = this.ppp.querySelector('[data-rwc-ppp-pes-h] b');
      this.pppQuotePesD = this.ppp.querySelector('[data-rwc-ppp-pes-d] b');
      this.pppQuotePesM = this.ppp.querySelector('[data-rwc-ppp-pes-m] b');

      this.pdfForm = this.ppp.querySelector('[data-rwc-pdf-form]');

      if (this.inputTroggers) {
        this.inputTroggers.forEach( input => { input.addEventListener('change', this.inputToggelStep.bind(this) ) });
      }
      
      if (this.ranges) {
        this.ranges.forEach( box => {
          let rangeValue; 

          const tooltip = box.querySelector('span');
          box.querySelector('input').addEventListener('input', (e) => {
            const inp = e.target;
            rangeValue = parseInt(e.target.value);
            tooltip.innerText = rangeValue;

            const rangeValuePs = (rangeValue - 1) * 25;
            const rangeValuePx = (rangeValue - 1) * 12;
            tooltip.style.left = `calc(${rangeValuePs}% - ${rangeValuePx}px)`;
          }); 
        });
      }

      this.nextBtn.addEventListener('click', this.goToNextStep.bind(this));
      calc.querySelectorAll('[data-trigger-quest]').forEach( input => { input.addEventListener('change', this.inputToggelQuestion.bind(this) ) });
      calc.querySelectorAll('input').forEach( input => { input.addEventListener('change', this.calcTTT.bind(this) ) });
      this.form.addEventListener('submit', this.submitForm.bind(this) );

      if (this.pppTrigger) {
        this.pppTrigger.addEventListener('change', this.togglePopup.bind(this) );
      }

      if (this.pdfForm) {
        this.pdfForm.addEventListener('submit', this.submitPdfForm.bind(this) );
      }
    }

    submitPdfForm(e) {
      e.preventDefault();
      this.pdfForm.classList.add('in--submit');
      const btn = this.pdfForm.querySelector('button');
      const btnText = btn.innerText;
      btn.innerText = 'Please, Wait...'

      let serialize = jQuery(this.form).serializeArray();
      let serializePdf = jQuery(this.pdfForm).serializeArray();


      _this = this;

      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'calculator_get_pdf',
            form: serialize,
            formPdf: serializePdf
        },
      }).done(function( data ) {
        console.log('data', data)
        
        _this.pdfForm.classList.remove('in--submit');
        btn.innerText = btnText;

        try {
					const answer = JSON.parse( data );
          console.log('answer', answer)

					if (answer.status == 'ok') {
						if (answer.redirect_url) window.location.href = answer.redirect_url;
					}

        } catch (error) {}
      });

    }


    togglePopup() {
      console.log('togglePopup', this.pppTrigger.checked);
      if (this.pppTrigger.checked) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = '';
      }
    }

    openPopup() {
      this.pppTrigger.checked = true;
      this.pppTrigger.dispatchEvent(new Event('change'));
    }

    calcTTT() {

      let serialize = jQuery(this.form).serializeArray();
      _this = this;

      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'cacl_calculation',
            form: serialize,
        },
      }).done(function( data ) {
          console.log('data', data)
          try {
            var answer = JSON.parse( data );
            if( answer.status == "ok" ){
              console.log('answer', answer)
              _this.tttMin.innerText = answer.min || 0;
              _this.tttMax.innerText = answer.max || 0;
            }
          } catch (error) {
          }
      });

    }

    submitForm(e) {
      e.preventDefault();
      this.form.classList.add('in--submit');

      let serialize = jQuery(this.form).serializeArray();
      _this = this;

      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'calculator_submit',
            form: serialize,
        },
      }).done(function( data ) {
          _this.form.classList.remove('in--submit');

          try {
            const answer = JSON.parse( data );
            _this.buildPopup(answer);
          } catch (error) {
          }
      });
    }

    buildPopup(info) {

      this.pppQuoteOptH.innerHTML = info.min_h;
      this.pppQuoteOptD.innerHTML = info.min_d;
      this.pppQuoteOptM.innerHTML = info.min_m;
      this.pppQuotePesH.innerHTML = info.max_h;
      this.pppQuotePesD.innerHTML = info.max_d;
      this.pppQuotePesM.innerHTML = info.max_m;

      if (info.packege) {
        this.pppPlan.innerHTML = info.html;
        this.ppp.classList.add('has--plan');
      } else {
        this.ppp.classList.remove('has--plan');
      }

      this.openPopup();
    }


    inputToggelStep(e) {
      const inp = e.target.dataset.triggerNext;

      if (inp) {
        if (e.target.checked && inp ) {
          this.openStep(inp);
        } else {
          this.closeStep(inp);
        }
      }
    }

    openStep(name) {
      this.calc.querySelector( `[data-step="${name}"]` ).classList.add('rqw-step--open');


      if (name == 'step21' || name == 'step22' || name == 'step23') {
        this.updateNextStep('step3'); 
      } else if (name == 'step3') { 
        this.updateNextStep('step4'); 
      } else if (name == 'step4' || name == 'step24') { 
        this.updateNextStep('step5'); 
      } else if (name == 'step5') { 
        this.updateNextStep('end');
        this.showSubmit = true;
        this.swicthSubmitVisibility();
      }
      this.swicthNextStepVisibility();

      if (name == 'step21' || name == 'step22' || name == 'step23' || name == 'step24') this.changeFirstStep();
    }
    
    closeStep(name) {
      this.calc.querySelector( `[data-step="${name}"]` ).classList.remove('rqw-step--open');
      this.changeFirstStep();
    }

    goToNextStep() {
      this.openStep(this.nextStep);
    }

    changeFirstStep() {
      let hasOpend = false;
      let hasBackend = false;
      let countDvice = 0;
      this.inputTroggers.forEach( input => { 
        if (input.checked) {
          hasOpend = true;
          countDvice++;
        }
        if (input.value == 'a-step1-0-3' && input.checked) { hasBackend = true; }
      });

      if (!hasOpend) {
        this.updateNextStep(false);
        this.showSubmit = false;
        this.swicthSubmitVisibility();
        this.swicthNextStepVisibility();

        ['step21','step22','step23','step3','step4','step24','step5'].forEach(step => {
          this.calc.querySelector( `[data-step="${step}"]` ).classList.remove('rqw-step--open');
        });
      }

      if (hasBackend && countDvice > 1 && this.nextStep) {
        ['step3','step4'].forEach(step => {
          this.calc.querySelector( `[data-step="${step}"]` ).classList.add('rqw-step--open');
        });
      }

      if (hasBackend && countDvice === 1) {
        ['step3','step4'].forEach(step => {
          this.calc.querySelector( `[data-step="${step}"]` ).classList.remove('rqw-step--open');
        });
      }

    }

    updateNextStep(step) {
      if (!step) {
        this.nextStep = false;
        return;
      }

      const a = {
        0 : false,
        3 : "step3", 
        4 : "step4", 
        5 : "step5",
        6 : "end",
      };

      let key_new_step = 0;
      let key_old_step = 0;

      for (const int in a) {
        if (Object.hasOwnProperty.call(a, int)) {
          const element = a[int];
          if (step == element) key_new_step = parseInt(int);
          if (this.nextStep == element) key_old_step = parseInt(int);
        }
      }

      if (key_new_step > key_old_step) 
      this.nextStep = step;
    }

    swicthSubmitVisibility() {
      if (this.showSubmit) {
        this.submitBox.classList.add('rqw-actions--show');
      } else {
        this.submitBox.classList.remove('rqw-actions--show');
      }
    }

    swicthNextStepVisibility() {
      if (this.nextStep && this.nextStep !== 'end') {
        this.nextBox.classList.add('rqw-actions--show');
      } else {
        this.nextBox.classList.remove('rqw-actions--show');
      }
    }

    inputToggelQuestion(e) {
      const target = e.target.dataset.triggerQuest;
      if (target) {
        this.calc.querySelectorAll(`[data-openhook="${target}"]`).forEach( t => {
          t.classList.toggle('open');
        })
      }
    }


  }
  

	class Calculator {
    
    constructor(calc) {
      this.calc = calc;
      this.form = calc.querySelector('[data-rw-calculator-form]');
      if (!this.form) return;

			
		}
	}


  document.querySelectorAll('[data-rw-calculator]').forEach( calc => new Calculator2(calc));
  
});

