let mix = require('laravel-mix');

mix
    .js('js/scripts.js', 'dist/script-calculator.js')
    .sass('scss/admin_style.scss', 'dist/admin_style.css')
    .sass('scss/style.scss', 'dist/style-calculator.css')
    .options({
        processCssUrls: false
    });

/*
mix.browserSync({
    proxy: 'mtg-wc.loc',
    files: ['*.php', 'views/!**!/!*.php', 'dist/app.css', 'dist/app.js']
});*/
