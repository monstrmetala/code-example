/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/scripts.js":
/*!***********************!*\
  !*** ./js/scripts.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

document.addEventListener('DOMContentLoaded', function () {
  var ajaxurl = '/wp-admin/admin-ajax.php';

  var Calculator = /*#__PURE__*/function () {
    function Calculator(calc) {
      var _this2 = this;

      _classCallCheck(this, Calculator);

      this.calc = calc;
      this.form = calc.querySelector('[data-rw-calculator-form]');
      if (!this.form) return;
      this.submitBox = calc.querySelector('[data-rw-submit]');
      if (!this.submitBox) return; // RANGE Handler

      calc.querySelectorAll('[data-rw-calculator-range]').forEach(function (box) {
        var rangeValue;
        var tooltip = box.querySelector('span');
        box.querySelector('input').addEventListener('input', function (e) {
          var inp = e.target;
          rangeValue = parseInt(e.target.value);
          tooltip.innerText = rangeValue;
          var rangeValuePs = rangeValue * 20; //* 25

          var rangeValuePx = (rangeValue - 1) * 12; //* 12

          tooltip.style.left = "calc(".concat(rangeValuePs, "% - ").concat(rangeValuePx, "px)");
        });
      }); // Time To Test

      this.tttMin = document.querySelector('[data-ttt-min]');
      this.tttMax = document.querySelector('[data-ttt-max]');
      calc.querySelectorAll('input').forEach(function (input) {
        input.addEventListener('change', _this2.calcTTT.bind(_this2));
      }); // Trigger Open - answer

      calc.querySelectorAll('[data-trigger-open]').forEach(function (input) {
        input.addEventListener('change', _this2.triggerOpenHandler.bind(_this2));
      }); // Trigger Open - question

      calc.querySelectorAll('[data-quest-trigger-open]').forEach(function (quest) {
        var inputs = quest.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.type === 'range') {
            input.addEventListener('input', function () {
              var targetId = quest.dataset.questTriggerOpen;

              var targetEl = _this2.calc.querySelector(".quest-id__".concat(targetId));

              if (targetEl) {
                if (input.value == '0') {
                  targetEl.classList.remove("quest-openby__".concat(quest.dataset.questKey));
                } else {
                  targetEl.classList.add("quest-openby__".concat(quest.dataset.questKey));
                }
              }
            });
          } else {
            input.addEventListener('change', function (inp) {
              var hasChecked = false;
              inputs.forEach(function (input) {
                if (input.checked) hasChecked = true;
              });
              var targetId = quest.dataset.questTriggerOpen;
              var targetEl;
              var triggerClaasName;

              if (targetId === '_finish') {
                targetEl = _this2.submitBox;
                triggerClaasName = 'rqw-actions--show';
              } else {
                targetEl = _this2.calc.querySelector(".quest-id__".concat(targetId));
                triggerClaasName = "quest-openby__".concat(quest.dataset.questKey);
              }

              if (targetEl) {
                if (hasChecked) {
                  targetEl.classList.add(triggerClaasName);
                } else {
                  targetEl.classList.remove(triggerClaasName);
                }
              }
            });
          }
        }); //  q.addEventListener('change', q.triggerOpenHandler.bind(this) ) 
      }); // ALL answer

      calc.querySelectorAll('[data-quest-has-all]').forEach(function (quest) {
        var inputAll = quest.querySelector('[data-answer-type="all"]');
        if (!inputAll) return;
        var otherInputs = quest.querySelectorAll('input:not([data-answer-type="all"])');
        inputAll.addEventListener('change', function () {
          if (inputAll.checked) {
            otherInputs.forEach(function (otherInput) {
              otherInput.checked = true;
            });
          }
        });
        /* If unchecked one of Other onput in pack */

        otherInputs.forEach(function (otherInput) {
          otherInput.addEventListener('change', function () {
            if (inputAll.checked && !otherInput.checked) {
              inputAll.checked = false;
            }
          });
        });
      });
    }

    _createClass(Calculator, [{
      key: "triggerOpenHandler",
      value: function triggerOpenHandler(event) {
        var triggerEl = event.target;
        var targetId = triggerEl.dataset.triggerOpen;
        var targetEl = this.calc.querySelector(".quest-id__".concat(targetId));

        if (targetEl) {
          targetEl.classList.toggle("quest-openby__".concat(triggerEl.value));
        }
      } // Time To Test - HANDLER

    }, {
      key: "calcTTT",
      value: function calcTTT() {
        var serialize = jQuery(this.form).serializeArray();
        _this = this;
        jQuery.ajax({
          url: ajaxurl,
          type: 'POST',
          data: {
            action: 'cacl_calculation',
            form: serialize
          }
        }).done(function (data) {
          try {
            var answer = JSON.parse(data);

            if (answer.status == "ok") {
              _this.tttMin.innerText = answer.min || 0;
              _this.tttMax.innerText = answer.max || 0;
            }
          } catch (error) {}
        });
      }
    }]);

    return Calculator;
  }();

  document.querySelectorAll('[data-rw-calculator]').forEach(function (calc) {
    return new Calculator(calc);
  });
});

/***/ }),

/***/ "./scss/admin_style.scss":
/*!*******************************!*\
  !*** ./scss/admin_style.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./scss/style.scss":
/*!*************************!*\
  !*** ./scss/style.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***********************************************************************!*\
  !*** multi ./js/scripts.js ./scss/admin_style.scss ./scss/style.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/redwek/Projects/QWCOM/wp-content/plugins/rqw-calculator/assets/js/scripts.js */"./js/scripts.js");
__webpack_require__(/*! /home/redwek/Projects/QWCOM/wp-content/plugins/rqw-calculator/assets/scss/admin_style.scss */"./scss/admin_style.scss");
module.exports = __webpack_require__(/*! /home/redwek/Projects/QWCOM/wp-content/plugins/rqw-calculator/assets/scss/style.scss */"./scss/style.scss");


/***/ })

/******/ });