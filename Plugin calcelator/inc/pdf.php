<?php
  use Spipu\Html2Pdf\Html2Pdf;
  use Spipu\Html2Pdf\Exception\Html2PdfException;
  use Spipu\Html2Pdf\Exception\ExceptionFormatter;

function generate_pdf($info = null, $dist = 'F') {
  $res = [];

	$li_init_path = PDF_LIB . '/vendor/autoload.php';
  if (!file_exists($li_init_path)) wp_die( 'lib NOT EXIST' );
  require_once( $li_init_path );
    
  try {
    $html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->pdf->SetDisplayMode('fullpage');

    ob_start();
      include CLC_PATH . '/PDF/pdf-template.php';
			// include PDF_LIB.'/examples/res/about.php';
    $content = ob_get_clean();

    $html2pdf->writeHTML($content);
    $html2pdf->output( PDF_FILES . '/qw-pdf__'.$info['ID'].'.pdf', $dist );
		$res['status'] = 'ok';
  } catch (Html2PdfException $e) {
		$html2pdf->clean();
		
    $formatter = new ExceptionFormatter($e);
		$res['status'] = 'false';
		ob_start();
		echo $formatter->getHtmlMessage();
		$res['error'] = ob_get_clean();
  }

  return $res;
}
