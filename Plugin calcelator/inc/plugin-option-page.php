<?php
include_once CLC_PATH . '/configs/dashboard_config.php';

add_action( 'admin_menu', 'register_menu_page', 10 );
add_action( 'admin_menu', 'register_submenu_page', 90 );

// Добавим видимость пункта меню для Редакторов
function register_menu_page(){
  add_menu_page( 'RW/QW Calculator', 'RW/QW Calculator', 'edit_others_posts', 'rqw_clc_menu_page', '', 'dashicons-building' );
}

function register_submenu_page() {
  add_submenu_page( 'rqw_clc_menu_page', 'Settings', 'Settings', 'edit_others_posts', 'rqw_clc_settings', 'rwclc_admin_menu', 90 );
}

function rwclc_admin_menu() {
  global $settings_page;

  if (is_array($_POST) && !empty($_POST) ) {
    save_rwcalc_page_options($_POST);
  }

  $rwclc_options = get_option('rwcalc');

  $table_start = '<form action="" method="post" class="rw-adm-options-form"><table class="form-table" role="presentation"><tbody>';
  $table_end = '</tbody></table><p class="submit"><button type="submit" class="button button-primary">Save</button></p></form>';

  echo $table_start;
  foreach ($settings_page as $block => $fields) {
    printf('<tr class="form-heading"><th><h2>%s</h2></th></tr>', $block);
    render_dashboard_fields($fields, $rwclc_options);
  }
  echo $table_end;
}

function render_dashboard_fields($fields = [], $rwclc_options) {
  foreach ($fields as $fld) {
    printf('<tr class="form-field"><th scope="row"><label for="basic_price">%s</label></th>', $fld['name'] );
    if ($fld['type'] == 'pack') {
      $puck_list = '';
      foreach ($fld['pack'] as $pkey => $pfield) {
        $field_name = $fld['slug'] .'['.$pfield['slug'].']';
        $field_value = isset($rwclc_options[ $fld['slug'] ][ $pfield['slug'] ]) ? $rwclc_options[ $fld['slug'] ][ $pfield['slug'] ] : '';

        $tpl = render_field_type_tpl($pfield);
        $puck_list .= sprintf( $tpl, $field_name, $field_value );
      }
      printf('<td class="form-field__pack">%s</td>', $puck_list);
    } else {

      $field_name = $fld['slug'];
      $field_value = isset($rwclc_options[ $field_name ]) ? $rwclc_options[ $field_name ] : '';

      $tpl = render_field_type_tpl($fld);
      printf('<td class="">'.$tpl.'</td>', $field_name, $field_value );
    }
    echo '</tr>'; 
  }
}

function render_field_type_tpl($f) {
  $tpl = '';
  switch ($f['type']) {
    case 'url':
    case 'email':
    case 'text':
      $tpl = sprintf(
        '<input name="%s" placeholder="%s" type="%s" class="regular-text" value="%s" >',
        '%s',
        esc_attr($f['placeholder']),
        $f['type'],
        '%s'
      );
    break;
    case 'number':
      $tpl = sprintf(
        '<input name="%s" placeholder="%s" type="%s" class="regular-text" value="%s" min="0" max="1000000" step="1">',
        '%s',
        esc_attr($f['placeholder']),
        $f['type'],
        '%s'
      );
    break;
    case 'textarea':
      $tpl = sprintf(
        '<textarea name="%s" placeholder="%s" class="regular-text" rows="5">%s</textarea>',
        '%s',
        esc_attr($f['placeholder']),
        '%s'
      );
    break;
  }

  return $tpl;
}


function save_rwcalc_page_options ($post_obj) {
  update_option('rwcalc', $post_obj);
}

