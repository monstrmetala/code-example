<?php
add_action('rw_calc_fire_emails', 'rw_calc_email_to_client', 10, 2);
function rw_calc_email_to_client ($post_id, $instanse) {
	$req_meta = get_post_meta($post_id);
	if (!$instanse['email']) return;

	// CLIENT EMAIL
	$rwclc_options = get_option('rwcalc');
	
	$to = $req_meta['client_email'];
	$subject = 'Your Software Testing - Rough Estimate by QAwerk';
	$headers = [
		'From: Konstantin Klyagin <konst@qawerk.net>',
		'content-type: text/html',
	];
	$attachments = null;
	$pdf_url = $req_meta['pdf_url'][0] ? $req_meta['pdf_url'][0] : '';
	$book_url = $rwclc_options['meet_url'] ? $rwclc_options['meet_url'] : '';


	if (file_exists(PDF_FILES . '/qw-pdf__' . $post_id . '.pdf')) {
		$attachments = PDF_FILES . '/qw-pdf__' . $post_id . '.pdf';
	}
		
	$msg = @file_get_contents( CLC_PATH . '/email-templates/client.php' );
	if ($msg && is_string($msg)) {

		$msg = str_replace('{{pdf_url}}', $pdf_url, $msg);
		$msg = str_replace('{{book_url}}', $book_url, $msg);
		$message = $msg;

		$send = wp_mail( $to, $subject, $message, $headers, $attachments );
		update_post_meta($post_id, 'send_mail_to_client', $send);
	}
}


add_action('rw_calc_fire_emails', 'rw_calc_email_to_admin', 15, 2);
function rw_calc_email_to_admin($post_id, $instanse) {
	$req_meta = get_post_meta($post_id);
	if (!$instanse['email']) return;
	
	$rwclc_options = get_option('rwcalc');
	if (!$rwclc_options['admin_email']) return;

	$to = $rwclc_options['admin_email'];
	$subject = 'I requested software testing cost';
	$headers = [
		'From: '.$instanse['email'].' <'.$instanse['email'].'>',
		'content-type: text/html',
	];
	$attachments = null;
	$pdf_url = $req_meta['pdf_url'][0] ? $req_meta['pdf_url'][0] : '';

	if (file_exists(PDF_FILES . '/qw-pdf__' . $post_id . '.pdf')) {
		$attachments = PDF_FILES . '/qw-pdf__' . $post_id . '.pdf';
	}
		
	$msg = @file_get_contents( CLC_PATH . '/email-templates/admin.php' );
	if ($msg && is_string($msg)) {

		$msg = str_replace('{{pdf_url}}', $pdf_url, $msg);
		
		$msg = str_replace('{{client_email}}', $instanse['email'], $msg);
		$msg = str_replace('{{ip}}'					 , $instanse['ip'], $msg);
		$msg = str_replace('{{contry}}'      , $instanse['country'], $msg);
		$msg = str_replace('{{page_title}}'  , $instanse['page_name'], $msg);
		$msg = str_replace('{{page_url}}'    , $instanse['page_url'], $msg);

		$message = $msg;

		$send = wp_mail( $to, $subject, $message, $headers, $attachments );
		update_post_meta($post_id, 'send_mail_to_client', $send);
	}
}
