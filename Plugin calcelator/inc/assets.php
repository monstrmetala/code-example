<?php

add_action('after_rw_calculator', 're_inline_assets', 10 );
function re_inline_assets() {
  $css = CLC_URL . 'assets/dist/style-calculator.css';
  $js  = CLC_URL . 'assets/dist/script-calculator.js';

  printf( 
    '<link rel="stylesheet" href="%s"><script src="%s"></script>',
    $css,
    $js
  );
}


/******************************************
*  			 A D M I N    E N Q U E U E 
*******************************************/

add_action( 'admin_enqueue_scripts', 'rw_calc_admin_assets', 50 );
function rw_calc_admin_assets(){
	// wp_enqueue_script('rw_calc_admin_js', CLC_URL . '/assets/dist/admin_script.js');
	wp_enqueue_style('rw_calc_admin_css', CLC_URL . '/assets/dist/admin_style.css');
}
