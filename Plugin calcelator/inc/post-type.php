<?php
add_action( 'init', 'clc_add_postype_request' );
function clc_add_postype_request() {
  register_post_type('calc_request', array(
		'labels'             => array(
			'name'               => 'Request', // Основное название типа записи
			'singular_name'      => 'Requests', // отдельное название записи типа Book
			'add_new'            => '+ Request',
			'add_new_item'       => '+ New Request',
			'edit_item'          => 'Edit Request',
			'new_item'           => 'New Request',
			'view_item'          => 'View Request',
			'search_items'       => 'Find Request',
			'not_found'          => 'Request Not found',
			'not_found_in_trash' => 'Trash is Empty',
			'parent_item_colon'  => '',
			'menu_name'          => 'Requests'
		  ),
		'public'             => true,
		'publicly_queryable' => false,
		'exclude_from_search' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'rqw_clc_menu_page',
		// 'show_in_menu'       => 'admin.php?page=rqw_clc_menu_page',
		'show_in_admin_bar'  => 'admin.php?page=rqw_clc_menu_page',
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 1,
		'supports'           => array('title','editor')
	) );
}

add_action('add_meta_boxes', 'rwcalc_request_meta');
function rwcalc_request_meta(){
	add_meta_box( 'myplugin_sectionid', 'Request Info', 'rwcalc_request_meta_callback', 'calc_request' );
}

function rwcalc_request_meta_callback( $post, $meta ){
	$pdf_url = get_post_meta( $post->ID, 'pdf_url', true);
	$pdf_path = get_post_meta( $post->ID, 'pdf_path', true);
	$send_mail_to_client = get_post_meta( $post->ID, 'send_mail_to_client', true);

	// pdf_url
	// pdf_path
	// send_mail_to_client
// $send_mail_to_admin = false;
// $autoreply_email = false;
	$m = [
		'PDF Url' => $pdf_url,
		'PDF Path' => $pdf_path,
		'Send e-mail to client' => $send_mail_to_client ? 'Send' : 'NOT Sent',
		'Send e-mail to admin' =>  $send_mail_to_admin ? 'Send' : 'NOT Sent',
		'Auto-reply e-mail (2h)' => $autoreply_email ? 'Send' : 'NOT Sent',
	];
	
	
	foreach ($m as $key => $value) {
		printf(
			'<div class="rw-request-meta"><div class="rw-request-meta__name">%s</div><div class="rw-request-meta__value">%s</div></div>',
			$key,
			$value
		);
	}
	
	
}
