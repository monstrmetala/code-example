<?php

function build_pdf_data ($i, $config) {
 
	$data = [];
	$totsl_deviced = 0;
	// MOB
	if (in_array('a-step1-0-0', $i['q-step1-0'])) {
		$data_mobile = [];

		// Base
		if (isset($i['q-step21-0'])) {
			foreach ($config['step21']['qss']['q-step21-0']['a'] as $a => $value) {
				if ($i['q-step21-0'][0] == $a) {
					$res = field_handler_array($value);
					if ($res) {
						$data_mobile['base'][0] 		= $res['min'];
						$data_mobile['base'][1] 		= $res['max'];
						$data_mobile['base']['mid'] = $res['mid'];
					}
				}
			}
		} else {
			if (isset($config['step21']['qss']['q-step21-0']['default'])){
				$def_key = $config['step21']['qss']['q-step21-0']['default'];
				if (isset($config['step21']['qss']['q-step21-0']['a'][$def_key])){

					$res = field_handler_array($config['step21']['qss']['q-step21-0']['a'][$def_key]);
					if ($res) {
						$data_mobile['base'][0] 		= $res['min'];
						$data_mobile['base'][1] 		= $res['max'];
						$data_mobile['base']['mid'] = $res['mid'];
					}
				}
			}
		} 
		// Screens
		if (isset($i['q-step21-1'])) {
			foreach ($config['step21']['qss']['q-step21-1']['a'] as $a => $value) {
				if ($i['q-step21-1'][0] == $a) {
					$data_mobile['screens'] = $value['calc'];
				}
			}
		} else {
			$data_mobile['screens'] = 10;
		}
		
		// devices
		if (isset($i['q-step21-3'])) {
			$d = (int)$i['q-step21-3'][0];
			$totsl_deviced += $d;
			if ($d > 1) {
				$dvc = [];
				$dvc[] = ( $data_mobile['base'][0] + intval($data_mobile['screens']) ) * 0.2 * ($d - 1);
				$dvc[] = ( $data_mobile['base'][1] + intval($data_mobile['screens']) ) * 0.2 * ($d - 1);
				$dvc[0] = round($dvc[0], 1);
				$dvc[1] = round($dvc[1], 1);
				$dvc[2] = round( ($dvc[0] + $dvc[1]) / 2 , 1);
				$data_mobile['devices'] = $dvc;
			}
		}

		$data['mob'] = $data_mobile; 
	} /** is mobile */


	// DESKTOP
  if (in_array('a-step1-0-1', $i['q-step1-0'])) {
		$data_desktop = []; 
		// Base
		if (isset($i['q-step22-0'])) {
			foreach ($config['step22']['qss']['q-step22-0']['a'] as $a => $value) {
				if ($i['q-step22-0'][0] == $a) {
					$qa = explode(',', $value['calc']);
					if (is_array($qa) && count($qa) == 2) {
						$data_desktop['base'] = $qa;
						$min = (float)$qa[0];
						$max = (float)$qa[1];
						$mid = ($min + $max)/2;
						$data_desktop['base']['mid'] = $mid;
					}
				}
			}
		} else {
			$data_desktop['base'] = [12,24,'mid' => 18];
		} 

		// Screens
		if (isset($i['q-step22-1'])) {
			foreach ($config['step22']['qss']['q-step22-1']['a'] as $a => $value) {
				if ($i['q-step22-1'][0] == $a) {
					$data_desktop['screens'] = $value['calc'];
				}
			}
		} else {
			$data_desktop['screens'] = 10;
		}

		// OSs		
		if (isset($i['q-step22-2']) ) {
			if (in_array('a-step22-2-3', $i['q-step22-2'])) {
				$os = 3;
			} else {
				$os = count($i['q-step22-2']);
			}
			
			if ($os > 1) {
				$dvc = [];
				$dvc[] = ( $data_desktop['base'][0] + intval($data_desktop['screens']) ) * 0.2 * ($os - 1);
				$dvc[] = ( $data_desktop['base'][1] + intval($data_desktop['screens']) ) * 0.2 * ($os - 1);
				$dvc[0] = round($dvc[0], 1);
				$dvc[1] = round($dvc[1], 1);
				$dvc[2] = round( ($dvc[0] + $dvc[1]) / 2 , 1);
				
				$data_desktop['os'] = $dvc;
			}

		}
		$data['desktop'] = $data_desktop; 
	}
	// WEB
  if (in_array('a-step1-0-2', $i['q-step1-0'])) {
		$data_web = []; 
		// BASE
		if (isset($i['q-step23-0'])) {
			foreach ($config['step23']['qss']['q-step23-0']['a'] as $a => $value) {
				if ($i['q-step23-0'][0] == $a) {
					$qa = explode(',', $value['calc']);
					if (is_array($qa) && count($qa) == 2) {
						$data_web['base'] = $qa;
						$min = (float)$qa[0];
						$max = (float)$qa[1];
						$mid = ($min + $max)/2;
						$data_web['base']['mid'] = $mid;
					}
				}
			}
		} else {
			$data_web['base'] = [12,24,18];
		} 
		// PAGES
		if (isset($i['q-step23-1'])) {
			foreach ($config['step23']['qss']['q-step23-1']['a'] as $a => $value) {
				if ($i['q-step23-1'][0] == $a) {
					$data_web['pages'] = $value['calc'];
				}
			}
		} else {
			$data_web['pages'] = 10;
		}
		
		if (isset($i['q-step23-2']) ) {
			$N_m = 0;
			$N_d = 0;

			if (in_array('a-step23-2-0', $i['q-step23-2'])) {
				$N_m = (int)$i['q-step23-3'][0];
			}
			if (in_array('a-step23-2-1', $i['q-step23-2'])) {
				$N_d = (int)$i['q-step23-5'][0];
			}

			$N = $N_m + $N_d;
			$totsl_deviced += $N;
			$data_web['N']['Nm'] = $N_m;
			$data_web['N']['Nd'] = $N_d;

			if ($N > 1) {
				$dvc = [];
				$dvc[] = ( $data_web['base'][0] + intval($data_web['screens']) ) * 0.2 * ($N - 1);
				$dvc[] = ( $data_web['base'][1] + intval($data_web['screens']) ) * 0.2 * ($N - 1);
				$dvc[0] = round($dvc[0], 1);
				$dvc[1] = round($dvc[1], 1);
				$dvc[2] = round( ($dvc[0] + $dvc[1]) / 2 , 1);
				
				$data_web['devices'] = $dvc;
			}
		}

		$B_m = 0;
		$B_d = 0;
		if (isset($i['q-step23-4']) ) {
			$B_m = count($i['q-step23-4']);
		}
		if (isset($i['q-step23-6']) ) {
			$B_d = count($i['q-step23-6']);
		}

		$B = $B_m + $B_d;
		if ($B > 1) {
			$dvc = [];
			$dvc[] = ( $data_web['base'][0] + intval($data_web['screens']) ) * 0.2 * ($B - 1);
			$dvc[] = ( $data_web['base'][1] + intval($data_web['screens']) ) * 0.2 * ($B - 1);
			$dvc[0] = round($dvc[0], 1);
			$dvc[1] = round($dvc[1], 1);
			$dvc[2] = round( ($dvc[0] + $dvc[1]) / 2 , 1);
			$data_web['browsers'] = $dvc;
		}

		$data_web['B']['Bm'] = $B_m;
		$data_web['B']['Bd'] = $B_d;

		$data['web'] = $data_web; 
	}
	// BACKEND
  if (in_array('a-step1-0-3', $i['q-step1-0'])) {
		$data_back = []; 

		if (isset($i['q-step24-0'])) {
			foreach ($config['step24']['qss']['q-step24-0']['a'] as $a => $value) {
				// if ($i['q-step24-0'][0] == $a) {
				if (in_array($a, $i['q-step24-0'])) {
					$qa = explode(',', $value['calc']);
					if (is_array($qa) && count($qa) == 2) {
						$data_back['base'] = $qa;
						$min = (float)$qa[0];
						$max = (float)$qa[1];
						$mid = ($min + $max)/2;
						$data_back['base']['mid'] = $mid;
					}
				}
			}
		} else {
			$data_back['base'] = [12,24,'mid' => 18];
		}
	
		if (isset($i['q-step24-1'])) {
			$data_back['kinds'] = [];

			foreach ($config['step24']['qss']['q-step24-1']['a'] as $a => $value) {
				if ($value['calc'] == '0') continue;

				if ( in_array($a, $i['q-step24-1']) ) {
					$k = [];
					$k['name'] = $value['text'];
					$k['time'] = [];

					$e = floatval($value['calc']);
					
					$k['time'][0] = round($data_back['base'][0] * $e, 1);
					$k['time'][1] = round($data_back['base'][0] * $e, 1);
					$k['time'][2] = round( ($k['time'][0] + $k['time'][1]) / 2  , 1);

					$data_back['kinds'][] = $k;
				} 
			}
		}

		$data['back'] = $data_back; 
	}


	$totsl_deviced = $totsl_deviced ? $totsl_deviced : 0;


	// FEATURES
	$data_features = [];
	if (isset($i['q-step3-0'])) {
		foreach ($config['step3']['qss']['q-step3-0']['a'] as $key => $value) {
			if (in_array($key, $i['q-step3-0'])) {
				$f = [];
				$f['name'] = $value['text'];
				$f['time'] = $totsl_deviced * floatval($value['calc']);
				$data_features[] = $f;
			}
		}
  }
	if (isset($i['q-step4-0'])) {
		foreach ($config['step4']['qss']['q-step4-0']['a'] as $key => $value) {
			if (in_array($key, $i['q-step4-0'])) {
				$f = [];
				$f['name'] = $value['text'];
				$f['time'] = $totsl_deviced * floatval($value['calc']);
				$data_features[] = $f;
			}
		}
  }

	if (!empty($data_features)) $data['features'] = $data_features;

	if (isset($i['q-step5-0'])) {
		$data_cycle = false;

		$cyc = [];

		if (in_array('a-step5-0-0', $i['q-step5-0'])) {
			$cyc['d'] = $config['step5']['qss']['q-step5-0']['a']['a-step5-0-0']['text'];
		}
		
		if (in_array('a-step5-0-1', $i['q-step5-0'])) {
			$cyc['p'] = $config['step5']['qss']['q-step5-0']['a']['a-step5-0-1']['text'];
		}

		if (count($cyc) == 2) {
			$data_cycle = '<span class="cicle-b">'. implode(' + ', $cyc).'</span>';
		} else if (isset($cyc['d'])) {
			$data_cycle = '<span class="cicle-d">'. $cyc['d'] .'</span>';
		} else if (isset($cyc['p'])) {
			$data_cycle = '<span class="cicle-p">'. $cyc['p'] .'</span>';
		}

		$data['cycle'] = $data_cycle;
  }

	return $data;
}
