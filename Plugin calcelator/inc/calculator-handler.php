<?php

function full_calculation($instanse) {
  $return = [0,0];
  include_once CLC_PATH . '/configs/calc_config.php';

	if (isset($instanse['q-step1-0'])) {
    $QuoteM = [0,0];
    $QuoteD = [0,0];
    $QuoteW = [0,0];
    $QuoteB = [0,0];

		$details = [];

    $X3 = rw_calc_step3($instanse, $config );
    $X4 = rw_calc_step4($instanse, $config );
    $X5 = rw_calc_step5($instanse, $config );

    if (in_array('a-step1-0-0', $instanse['q-step1-0'])) {
			$MOB = rw_calc_mob($instanse, $config);
      $QuoteM = rw__qute_m($MOB, $X3, $X4, $X5);
    }
    if (in_array('a-step1-0-1', $instanse['q-step1-0'])) {
      $DESK = rw_calc_desk($instanse, $config);
      $QuoteD = rw__qute_d($DESK, $X3, $X4, $X5);
    }
    if (in_array('a-step1-0-2', $instanse['q-step1-0'])) {
      $WEB = rw_calc_web($instanse, $config);
      $QuoteW = rw__qute_w($WEB, $X3, $X4, $X5);
    }
    if (in_array('a-step1-0-3', $instanse['q-step1-0'])) {
      $BACK = rw_calc_back($instanse, $config);
      $QuoteB = rw__qute_b($BACK, $X5);
    }

    $QUOTE = [0,0];
    $QUOTE[0] = $QuoteM[0] + $QuoteD[0] + $QuoteW[0] + $QuoteB[0];
    $QUOTE[1] = $QuoteM[1] + $QuoteD[1] + $QuoteW[1] + $QuoteB[1];

    $return[0] = floor($QUOTE[0]);
    $return[1] = floor($QUOTE[1]);


		
		if ($QuoteM[0] != 0 && $QuoteM[1] != 0) $details['mob'] = $QuoteM;
		if ($QuoteD[0] != 0 && $QuoteD[1] != 0) $details['desk'] = $QuoteD;
		if ($QuoteW[0] != 0 && $QuoteW[1] != 0) $details['web'] = $QuoteW;
		if ($QuoteB[0] != 0 && $QuoteB[1] != 0) $details['back'] = $QuoteB;
			
    $return[2] = $details;

		ob_start();
			echo '<pre>$MOB<br />'; var_dump($MOB); echo '</pre>';
			echo '<pre>$DESK<br />'; var_dump($DESK); echo '</pre>';
			echo '<pre>$WEB<br />'; var_dump($WEB); echo '</pre>';
			echo '<pre>$BACK<br />'; var_dump($BACK); echo '</pre>';
		$return['debug'] = ob_get_clean();
  }
	return $return;
}


function rw__qute_m($MOB, $X3, $X4, $X5) {
  $QA =$MOB['QA'];
  $P = $MOB['P'];
  $N = $MOB['N'];
  $F = ($X3 + $X4) * $N * $X5;

  $q_min = ($QA[0] + $P) * (1 + 0.2 * ($N - 1)) + $F;
  $q_max = ($QA[1] + $P) * (1 + 0.2 * ($N - 1)) + $F;
	$q_mid = ($q_min + $q_max) / 2; $q_mid = round($q_mid, 1);

  return [$q_min, $q_max, $q_mid];
  #   Quote mobile = (QA+P) x (1+ 0,2x (N-1))  + F
}

function rw__qute_d($DESK, $X3, $X4, $X5) {
  $QA =$DESK['QA'];
  $P = $DESK['P'];
  $N = $DESK['N'];
  $F = ($X3 + $X4) * $N * $X5;

  $q_min = ($QA[0] + $P) * (1 + 0.2 * ($N - 1)) + $F;
  $q_max = ($QA[1] + $P) * (1 + 0.2 * ($N - 1)) + $F;
	$q_mid = ($q_min + $q_max) / 2; $q_mid = round($q_mid, 1);

  return [$q_min, $q_max, $q_mid];
  #   Quote mobile = (QA+P) x (1+ 0,2x (N-1))  + F
}

function rw__qute_w($WEB, $X3, $X4, $X5) {
  $QA =$WEB['QA'];
  $P = $WEB['P'];
  $N = $WEB['N'];
  $B = $WEB['B'];
  $F = ($X3 + $X4) * $N * $X5;

  $q_min = ($QA[0] + $P) * (1 + 0.2 * ($N - 1)) * (1 + 0.2 * ($B - 1)) + $F;
  $q_max = ($QA[1] + $P) * (1 + 0.2 * ($N - 1)) * (1 + 0.2 * ($B - 1)) + $F;

	$q_mid = ($q_min + $q_max) / 2; $q_mid = round($q_mid, 1);

  return [$q_min, $q_max, $q_mid];
  #  (QA+P) x (1+ 0,2x (N-1)) х (1+0,2х (B-1)) + F
}

function rw__qute_b($BACK, $X5) {
  $QA =$BACK['QA'];
  $E = $BACK['E'];

  $q_min = $QA[0] + $QA[0] * $E * $X5;
  $q_max = $QA[1] + $QA[0] * $E * $X5;
	$q_mid = ($q_min + $q_max) / 2; $q_mid = round($q_mid, 1);

  return [$q_min, $q_max, $q_mid];
  #  Quote backend = QA + QAxE
  #  if both for Backend +QAxEx2 hrs
}


function rw_calc_mob($i, $config) {
  $QA = [0, 0];
  $P = 10;
  $N = 1;

	// Default Value
	if (isset($config['step21']['qss']['q-step21-0']['default'])){
		$def_key = $config['step21']['qss']['q-step21-0']['default'];
		if (isset($config['step21']['qss']['q-step21-0']['a'][$def_key])){

			$res = field_handler_array($config['step21']['qss']['q-step21-0']['a'][$def_key]);
			if ($res) {
				$QA[0] 		= $res['min'];
				$QA[1] 		= $res['max'];
			}
		}
	}


  if (isset($i['q-step21-0'])) {
		foreach ($config['step21']['qss']['q-step21-0']['a'] as $a => $value) {
			if ($i['q-step21-0'][0] == $a) {
				$qa = explode(',', $value['calc']);
				if (is_array($qa) && count($qa) == 2) $QA = $qa;
			}
		}
  } 

  if (isset($i['q-step21-1'])) {
		foreach ($config['step21']['qss']['q-step21-1']['a'] as $a => $value) {
			if ($i['q-step21-1'][0] == $a) {
				$p = $value['calc'];
				if (is_string($p) && !empty($p)) $P = (int)$p;
			}
		}
  }

  if (isset($i['q-step21-3'])) {
    $N = (int)$i['q-step21-3'][0];
  }

  return [
    'QA' => $QA,
    'P' => $P,
    'N' => $N
  ];

}

function rw_calc_desk($i, $config) {
  $QA = [12, 24];
  $P = 10;
  $N = 1;

  if (isset($i['q-step22-0'])) {
		foreach ($config['step22']['qss']['q-step22-0']['a'] as $a => $value) {
			if ($i['q-step22-0'][0] == $a) {
				$qa = explode(',', $value['calc']);
				if (is_array($qa) && count($qa) == 2) $QA = $qa;
			}
		}
  } 

  if (isset($i['q-step22-1'])) {
		foreach ($config['step22']['qss']['q-step22-1']['a'] as $a => $value) {
			if ($i['q-step22-1'][0] == $a) {
				$p = $value['calc'];
				if (is_string($p) && !empty($p)) $P = (int)$p;
			}
		}
  }

  if (isset($i['q-step22-2']) ) {
    if (in_array('a-step22-2-3', $i['q-step22-2'])) {
      $N = 3;
    } else {
      $N = count($i['q-step22-2']);
    }
  }

  return [
    'QA' => $QA,
    'P' => $P,
    'N' => $N
  ];
}

function rw_calc_web($i, $config) {
  $QA = [12, 24];
  $P = 10;
  $N = 1;
  $B = 1;

  if (isset($i['q-step23-0'])) {
		foreach ($config['step23']['qss']['q-step23-0']['a'] as $a => $value) {
			if ($i['q-step23-0'][0] == $a) {
				$qa = explode(',', $value['calc']);
				if (is_array($qa) && count($qa) == 2) $QA = $qa;
			}
		}
  } 

  if (isset($i['q-step23-1'])) {
		foreach ($config['step23']['qss']['q-step23-1']['a'] as $a => $value) {
			if ($i['q-step23-1'][0] == $a) {
				$p = $value['calc'];
				if (is_string($p) && !empty($p)) $P = (int)$p;
			}
		}
  }

  $N_m = 0;
  $N_d = 0;

  $B_m = 0;
  $B_d = 0;

  if (isset($i['q-step23-2']) ) {
    if (in_array('a-step23-2-0', $i['q-step23-2'])) {
      $N_m = (int)$i['q-step23-3'][0];
    }
    if (in_array('a-step23-2-1', $i['q-step23-2'])) {
      $N_d = (int)$i['q-step23-5'][0];
    }
    $N = $N_m + $N_d;
  }

  if (isset($i['q-step23-4']) ) {
    $B_m = count($i['q-step23-4']);
  }

  if (isset($i['q-step23-6']) ) {
    $B_d = count($i['q-step23-6']);
  }

  $B = $B_m + $B_d;

  return [
    'QA' => $QA,
    'P' => $P,
    'N' => ($N == 0) ? 1 : $N,
    'B' => ($B == 0) ? 1 : $B
  ];
}

function rw_calc_back($i, $config) {
  $QA = [12, 24];
  $E = 0;

  if (isset($i['q-step24-0'])) {
		foreach ($config['step24']['qss']['q-step24-0']['a'] as $a => $value) {
			if ($i['q-step24-0'][0] == $a) {
				$qa = explode(',', $value['calc']);
				if (is_array($qa) && count($qa) == 2) $QA = $qa;
			}
		}
  } 

  if (isset($i['q-step24-1'])) {
		foreach ($config['step24']['qss']['q-step24-1']['a'] as $a => $value) {
			if (in_array($a, $i['q-step24-1'])) {
				$e = $value['calc'];
				$E += floatval($e);
			}
		}
  }

  return [
    'QA' => $QA,
    'E' => $E,
  ];

}

function rw_calc_step3($i, $config ) {
  $index = 0;
	
  if (isset($i['q-step3-0'])) {
		foreach ($config['step3']['qss']['q-step3-0']['a'] as $key => $value) {
			if (in_array($key, $i['q-step3-0'])) {
				$str_to_float = floatval($value['calc']);
				$index += $str_to_float; 
			}
		}
  }

  return $index;
}

function rw_calc_step4($i, $config ) {
  $index = 0;

  if (isset($i['q-step4-0'])) {
		foreach ($config['step4']['qss']['q-step4-0']['a'] as $key => $value) {
			if (in_array($key, $i['q-step4-0'])) {
				$str_to_float = floatval($value['calc']);
				$index += $str_to_float; 
			}
		}
  }

  return $index;
}

function rw_calc_step5($i, $config) {
  $index = 0;

  if (isset($i['q-step5-0'])) {
		foreach ($config['step5']['qss']['q-step5-0']['a'] as $key => $value) {
			if ($key == 'a-step5-0-2') continue;
			if (in_array($key, $i['q-step5-0']))  {
				$index+=1; 
			}
		}
  }

  return ($index == 0) ? 1 : $index;
}
