<?php

add_shortcode('rw_calculator', 'render_rw_calculator');
function render_rw_calculator ($arg) {
  $arg = shortcode_atts( array(
		'foo' => 'no foo',
		'baz' => 'default baz'
	), $arg );

	ob_start();
	build_calculator();
  return ob_get_clean();
}



add_shortcode( 'rw_svg', 'rw_svg__shortcode_handler' );
function rw_svg__shortcode_handler($arg) {
  $arg = shortcode_atts( array(
		'svg' => null
	), $arg );
	
	global $SVG;

	if (isset( $SVG[ 'svg-' . $arg['svg'] ] )) {
		
		return sprintf( '<span class="rw-svg">%s</span>',
			$SVG[ 'svg-' . $arg['svg'] ]
		);

	}
}
  
