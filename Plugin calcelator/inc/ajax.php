<?php


add_action( 'wp_ajax_cacl_calculation', 'cacl_calculation_via_ajax' );
add_action( 'wp_ajax_nopriv_cacl_calculation', 'cacl_calculation_via_ajax' );
function cacl_calculation_via_ajax(){
	$return = array();
	$return['status'] = 'false';
	$return['message'] = 'Fail!';

  if (isset($_POST['form']) && !empty($_POST['form'])) {

    $Q = do_calculation($_POST['form']);

    $return['min'] = $Q[0];
    $return['max'] = $Q[1];

    $return['status'] = 'ok';
  }

  echo json_encode($return);
	die;
}
 
add_action( 'wp_ajax_calculator_submit', 'calculator_submit_via_ajax' );
add_action( 'wp_ajax_nopriv_calculator_submit', 'calculator_submit_via_ajax' );
function calculator_submit_via_ajax(){
	$return = array();
	$return['status'] = 'false';
  $return['packege'] = false;

  if (isset($_POST['form']) && !empty($_POST['form'])) {
    $Q = do_calculation($_POST['form']);
 
    $delta = 100;
    $rait = 30;
    $price_min = $Q[0] * $rait - $delta;
    $price_max = $Q[1] * $rait + $delta;


  	$return['a0'] = $price_min;
  	$return['a1'] = $price_max;

    $in_packege = false;
    $in_packege_name = '';
    
    $rwclc_options = get_option('rwcalc');

    $pckgs = ['basic_packege','standart_packege','pro_packege','ultimate_packege'];
    foreach ($pckgs as $pkg) {
      if (isset($rwclc_options[$pkg]) && isset($rwclc_options[$pkg]['price']) ) {
        $pkg_price = (int)$rwclc_options[$pkg]['price'];
        if(!$pkg_price) continue;
        
        if ( $price_min <= $pkg_price && $pkg_price <= $price_max ) {
          $return['packege'] = true;
          $in_packege = $rwclc_options[$pkg];
          $in_packege_name = $pkg;
          break;
        }
      }
    }

    // Generate packege card HTML for popup 
    if ($return['packege'] && $in_packege && $in_packege_name) {
      $return['html'] = rw_calc_get_pkg_carg_html($in_packege, $in_packege_name);
    }

    $return['min_h'] = $Q[0];
    $return['min_d'] = ceil( $Q[0] / 8 );
    $return['min_m'] = round( $return['min_d'] / 20 , 1);

    $return['max_h'] = $Q[1];
    $return['max_d'] = ceil( $Q[1] / 8 );
    $return['max_m'] = round( $return['max_d'] / 20 , 1 );

    $return['status'] = 'ok';
  }

  echo json_encode($return);
	die;
}

function rw_calc_get_pkg_carg_html($pkg, $name) {
  global $SVG;

  $pckgs = [
    1 => 'basic_packege',
    2 => 'standart_packege',
    3 => 'pro_packege',
    4 => 'ultimate_packege'
  ];
  
  $svg_index = 0;
  foreach ($pckgs as $key => $p) {
    if ($name == $p) {
      $svg_index = 'svg-plan-' . $key;
    }
  }
  
  $html_ico = '';
  if (isset($SVG[$svg_index])) {
    $html_ico = $SVG[$svg_index];
  }

  $html_name = $pkg['name'] ? $pkg['name'] : '';
  $html_price = $pkg['price'] ? '$'.$pkg['price'] : '';

  $lis = '';
  if ($pkg['info']) {
    $li_a = explode(PHP_EOL, $pkg['info']);
    if (is_array($li_a) && !empty($li_a)) {
      $lis .= '<ul class="rwc-plan__info">';
      foreach ($li_a as $li) {
        $lis .= '<li>'.$li.'</li>';
      }
      $lis .= '</ul>';
    }
  }


  return sprintf(
    '<div class="rwc-plan__ico">%s</div>
    <span class="rwc-plan__subtitle">Manual</span>
    <span class="rwc-plan__name">%s</span>
    <span class="rwc-plan__price">%s</span>
    %s',
    $html_ico,
    $html_name,
    $html_price,
    $lis
  );

}

add_action( 'wp_ajax_calculator_get_pdf', 'calculator_get_pdf_via_ajax' );
add_action( 'wp_ajax_nopriv_calculator_get_pdf', 'calculator_get_pdf_via_ajax' );
function calculator_get_pdf_via_ajax(){
  $return = array();
	$return['status'] = 'false';

	$instanse = [];
	foreach ($_POST['formPdf'] as $value) {
		$instanse[$value['name']] = $value['value'];
	}
	
  $client_email = false;
  if (isset($instanse['email']) && !empty($instanse['email'])) {
    $client_email = esc_attr($instanse['email']);
  }

  if (isset($_POST['form']) && !empty($_POST['form']) && $client_email ) {

    $Q = do_calculation($_POST['form']);
		$pdf_data = get_pdf_data($_POST['form']);
		
    $return['min'] = $Q[0];
    $return['max'] = $Q[1];

		$pdf_data['time_min'] = $return['min'];
		$pdf_data['time_max'] = $return['max'];

		$_round = ($Q[0] + $Q[1])/2;
		$pdf_data['time_middle'] = round( $_round, 1 );

		// CALCULATE PRICE
		$rwclc_options = get_option('rwcalc');
		if (is_array($rwclc_options) && isset($rwclc_options['rate']) && !empty($rwclc_options['rate'])) {
			$rate = intval($rwclc_options['rate']);
			
			$pdf_data['price_min'] = floatval($pdf_data['time_min']) * $rate;
			$pdf_data['price_max'] = floatval($pdf_data['time_max']) * $rate;
			$pdf_data['price_mid'] = floatval($pdf_data['time_middle']) * $rate;
			$pdf_data['price_min'] = round($pdf_data['price_min'], 1);
			$pdf_data['price_max'] = round($pdf_data['price_max'], 1);
			$pdf_data['price_mid'] = round($pdf_data['price_mid'], 1);
		} else {
			$pdf_data['price_min'] = '';
			$pdf_data['price_max'] = '';
			$pdf_data['price_mid'] = '';
			$pdf_data['price_min'] = '';
			$pdf_data['price_max'] = '';
			$pdf_data['price_mid'] = '';
		}

		// REDIRECT URL
		$pdf_data['meet_url'] = $rwclc_options['meet_url'] ? $rwclc_options['meet_url'] : false;
		$return['redirect_url'] = $rwclc_options['redirect_url'] ? $rwclc_options['redirect_url'] : false;

		// SOFTWARE TESTING
		$pdf_data['software'] = $Q[2];


    $req_title = 'Calculator request | '.$client_email.' | '. date('d.m.Y H:i:s');
    
    // $req_content = get_req_content();
    $req_content = '';
    $req_content .= '<p><strong>Min</strong> ' . $Q[0] . '</p>';
    $req_content .= '<p><strong>Max</strong> ' . $Q[1] . '</p>';
    $req_content .= '<p><strong>Client Email</strong> ' . $client_email . '</p>';

    // вставляем запись в базу данных
    $post_id = wp_insert_post( 
      wp_slash( 
        array(
          'post_title'    => sanitize_text_field( $req_title ),
          'post_content'  => $req_content,
          'post_status'   => 'draft',
          'post_type'     => 'calc_request',
          'ping_status'   => get_option('default_ping_status'),
          'post_parent'   => 0,
          'menu_order'    => 0,
          'meta_input'    => [ 
            'form_instance'   => json_encode($_POST['form']),
            'client_email'    => $client_email
          ],
        )
      )
    );

    if ($post_id) {
			$pdf_data['ID'] = $post_id;
      $pdf_url = build_pdf($pdf_data);
			if ($pdf_url['status'] == 'ok') {
				update_post_meta($post_id, 'pdf_url', $pdf_url['url']);
				update_post_meta($post_id, 'pdf_path', PDF_FILES . '/qw-pdf__' . $post_id . '.pdf' );

				do_action('rw_calc_create_request', $post_id);
				do_action('rw_calc_fire_emails', $post_id, $instanse);

				wp_update_post([
					'ID' => $post_id,
					'post_content' => $req_content . '<p><strong>PDF Url</strong> ' . $pdf_url['url']
				]);

				$return['status'] = 'ok';
			} else {
				$return['error'] = 'Built PDF Failed. Message: ' . $pdf_url['error'];
			}
      
    } else {
			$return['error'] = 'Post has not been created.';
		}
  }
  echo json_encode($return);
	die;
}

function get_pdf_data ($arg) {

	$instanse = [];    
  foreach ($arg as $value) {
    $instanse[$value['name']][] = $value['value'];
  }
  include_once CLC_PATH . '/inc/pdf-handler.php';
  include CLC_PATH . '/configs/calc_config.php';
	
	return build_pdf_data($instanse, $config);
}

function do_calculation($_post = [] ) {
  if (!$_post) return [0,0];
  $return = [0,0];
  include_once CLC_PATH . '/inc/calculator-handler.php';
  $instanse = [];    
  foreach ($_post as $value) {
    $instanse[$value['name']][] = $value['value'];
  }
	if (function_exists('full_calculation')) {
		$return = full_calculation( $instanse );
	}
  return $return;
}


function build_pdf($arg) {
  include_once CLC_PATH . '/inc/pdf.php';

  $res = [];
  $res['status'] = false;

  $res_pdf = generate_pdf($arg);
  if(is_array($res_pdf)) {

    if ($res_pdf['status'] == 'ok'){
      $res['status'] = 'ok';
      $res['url'] = CLC_URL . 'PDF/files/qw-pdf__'.$arg['ID'].'.pdf';
    } else {
			$res['error'] = 'generate_pdf() Failed. Message: '.$res['error'];
		}

  } else {
    $res['error'] = 'generate_pdf() Failed.';
  }

  return $res;
}
