<?php
	get_header();
	global $SVG;

	$slider_enabled = get_field('slider_enabled');
	if ($slider_enabled) {
		$slider = get_field('slider');
	}
	$links = get_field('links');
	$possts_title = get_field('possts_title');
	$blog_posts = get_field('blog_posts');
	$graph_title = get_field('graph_title');

?>

<main id="main" class="homepage">
	<?php if ($slider_enabled && $slider) :?>
	<section class="section-home-slider">
		<div class="slider slider__pre-init" data-slider>

			<?php
				foreach ($slider as $slide) {
					# Logo
					$logo_html = '';
					if ($slide['logo']) {
						$logo_html = sprintf( '<div class="slide__logo"><img src="%s" alt="Logo"></div>', $slide['logo']['sizes']['medium'] );
					}

					# Title
					$title_html = '';
					if ($slide['title']) {
						$title_html = sprintf( '<h2 class="slide__title">%s</h2>', $slide['title'] );
					}

					# SubTitle
					$subtitle_html = '';
					if ($slide['subtitle']) {
						$subtitle_html = sprintf( '<h3 class="slide__subtitle">%s</h3>', $slide['subtitle'] );
					}

					# Button
					$btn_html = '';
					if ($slide['button_label'] && $slide['button_url']) {
						$btn_html = sprintf( '<div class="slide__btn"><a class="btn btn--slider" href="%s" title="%s" >%s</a></div>',
							$slide['button_url'],
							esc_attr($slide['button_label']),
							$slide['button_label']
						);
					}

					# Background
					$bg_html = '';
					if ($slide['background']) {
						$bg_html = sprintf( '<div class="slide__bg"><img src="%s" alt="Background"></div>', $slide['background']['url'] );
					}

					printf( '<div class="slide" ><div class="row slide__content">%s</div>%s</div>',  $logo_html . $title_html . $subtitle_html . $btn_html, $bg_html );
				}
			?>

		</div>
	</section>
	<?php endif; ?>


	<?php if (is_array($links) && !empty($links)) : ?>
	<section>
		<div class="row">
			<div class="home-links">
				<?php foreach ($links as $link) {
					if (!$link['page']) continue;

					$url = get_permalink( $link['page']->ID );
					$link_ico_html = '';
					if ($link['icon']['url']) {
						$link_ico_html = sprintf( '<img src="%s" alt="%s">', $link['icon']['url'], esc_attr( $link['page']->post_title) );
					}

					printf(
						'<a class="home-links__link" href="%s" title="%s">%s<span class="label">%s</span></a>',
						$url,
						esc_attr( $link['page']->post_title ),
						$link_ico_html,
						$link['page']->post_title
					);
				}?>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if (is_array($blog_posts) && !empty($blog_posts)) : ?>
	<section class="home-latest-news">
		<div class="row">
			<h2 class="section-title section-title--has-octa"><span class="octa octa--in-title"><?php echo $SVG['octa'] ?></span><?php echo $possts_title; ?></h2>
			<ul class="latest-news">
				<?php
					$marker_url = _img('post-marker.png');
					foreach ($blog_posts as $key => $value) {

						printf(
							'<li><a href="%s" title="%s" ><img src="%s" alt="%s"><span>%s</span></a></li>',
							get_permalink( $value->ID ),
							esc_attr( $value->post_title ),
							$marker_url,
							esc_attr( $value->post_title ),
							$value->post_title
						);
					}
				?>
			</ul>
		</div>
	</section>
	<?php endif; ?>


	<section class="home-graphic">
		<div class="row">
			<h2 class="section-title section-title--has-octa"><span class="octa octa--in-title"><?php echo $SVG['octa'] ?></span><?php echo $graph_title; ?></h2>
			<?php render_graph(); ?>
		</div>
	</section>

</main>

<?php get_footer(); ?>
