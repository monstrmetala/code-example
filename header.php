<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>

	<!-- start:global -->
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ff5e14">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ff5e14"> -->


	<!-- Title tag :start -->
	<title><?php wp_title(''); ?><?php if (wp_title('', false)) {echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<!-- Title tag :end -->

	<!-- add 10.01.2019 -->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<meta name="format-detection" content="telephone=no">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Red+Hat+Display:wght@400;900&display=swap" rel="stylesheet" >

	<!-- start:wp_head -->
	<?php wp_head(); ?>
	<!-- end:wp_head -->
</head>

<?php global $SVG; ?>

<body <?php body_class() ?> itemscope itemtype="http://schema.org/WebPage">
	<header class="header">
		<div class="row header__row">

			<div class="header__logo">
				<a href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo('name') ); ?>" class="site-logo">
					<span class="octa site-logo__octa"><?php echo $SVG['octa'] ?></span>
					<span class="site-logo__textlogo">Options</span>
					<span class="site-logo__textlogo--bold">TradingClub</span>
				</a>
			</div>

			<div class="header__menu">
				<?php render_menu('main-menu'); ?>
				<label class="burger" for="toggle-mm"><?php echo $SVG['bar'] ?></label>
			</div>

		</div>
	</header>

	<?php get_template_part('templates/part/mobile-menu'); 	?>
