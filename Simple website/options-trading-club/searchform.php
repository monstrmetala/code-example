<!-- search -->
<?php 
	$id = rand(100000, 999999);
?>
<form class="search b20s" method="get" action="<?php echo home_url(); ?>" role="search">
	<label for="search_<?php echo $id?>">Search for:</label>
	<input class="search-input" id="search_<?php echo $id?>" type="search" name="s" placeholder="Search & hit enter…">
	<!-- <button class="search-submit" type="submit" role="button" title="Search">Search</button> -->
</form>
<!-- /search -->
