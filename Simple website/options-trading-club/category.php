<?php get_header();
	$head_title = 'Category: '.get_cat_name(get_query_var('cat'));
?>

<main id="main">
	<div class="row">

		<div class="loop_and_sbare">

			<div class="posts_in_loop">
				<h2 class="title-in-archive"><span><?php echo $head_title; ?></span></h2>

				<?php
					if (have_posts()) :
						get_template_part( 'loop' );
					endif;
				?>

				<div class="loop_nav">
					<div class="loop_nav_box">
						<?php
							$arg = array(
								'prev_text'          => __( '« Previous' ),
								'next_text'          => __( 'Next »' ),
								'screen_reader_text' => ' ',
							);
							echo get_the_posts_pagination( $arg );
						?>
					</div>
				</div>
			</div>

			<aside class="b20-sidebar">
				<?php dynamic_sidebar('main-sidebar'); ?>
			</aside>

		</div>
	</div>

</main>

<?php get_footer(); ?>
