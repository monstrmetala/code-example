<?php

/* Theme options */
add_action('admin_menu', function(){
	add_menu_page( 'HG Theme options', 'Theme options', 'manage_options', 'site-options', 'add_my_setting', '', 94 );
} );

function add_my_setting(){
	?>
	<div class="wrap">
			<h1><?php echo get_admin_page_title() ?></h1>

			<table class="form-table">
				<tbody>
					<tr >
						<th><label for="description">Clean cach</label></th>
						<td>
							<span class="ccach_btn">
								<button class="do_clean_cach_btn">Start</button>
							</span>
							<p class="description">Clean all menu cach.</p>
						</td>
					</tr>
				</tbody>
			</table>
    <?php
}
