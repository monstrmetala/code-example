<?php

/******************************************
*  			 S  C  R  I  P  T  S
*******************************************/

add_action('wp_enqueue_scripts', 'theme_header_scripts_dereg'); // Add Custom Scripts to wp_head
function theme_header_scripts_dereg(){
	if( !is_user_logged_in() ){
	// wp_deregister_script('jquery');
	// wp_dequeue_script('jquery');
		// wp_dequeue_script('jquery-migrate');
		// wp_deregister_script('jquery-migrate');
	}

}


// add_action('wp_enqueue_scripts', 'theme_header_scripts'); // Add Custom Scripts to wp_head
add_action('get_footer', 'theme_header_scripts'); // Add Custom Scripts to wp_head
function theme_header_scripts(){
	// wp_enqueue_script('stickyfill', get_template_directory_uri() . '/js/lib/stickyfill.min.js', array(), '', true); // stickyfill
	wp_enqueue_script('objectfitfill', get_template_directory_uri() . '/js/lib/object-fit-polyfill.js', array(), '', true);
	wp_enqueue_script('slick_js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '1.8.1', true);
	wp_enqueue_script('theme_script',    get_template_directory_uri() . '/js/scripts.js', array('jquery', 'slick_js' ), '', true);
}


/******************************************
*  			 S  T  Y  L  E
*******************************************/

add_action('wp_enqueue_scripts', 'theme_style_dereg'); // Add Theme Stylesheet
function theme_style_dereg (){

	if( !is_user_logged_in() ){
		wp_deregister_style('dashicons');
		wp_dequeue_style('dashicons');

		wp_deregister_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library' );

		wp_dequeue_style('admin-bar');
		wp_deregister_style('admin-bar');
	}

	if (is_front_page() || is_home()) {
		wp_enqueue_style('slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1', 'all');
	}

	wp_enqueue_style('normalize', get_template_directory_uri() . '/css/normalize.min.css', array(), '1.0', 'all');
	// wp_enqueue_style('head_theme_css', get_template_directory_uri() . '/dist/style_head.css', array(), '1.0', 'all');
}

add_action('get_footer', 'theme_style'); // Add Theme Stylesheet
function theme_style(){
	wp_enqueue_style('base_theme_css', 	get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
	wp_enqueue_style('theme-style',     get_template_directory_uri() .'/dist/style.css');
}
// add_action( 'wp_head', function() {
// 	$css = file_get_contents( get_template_directory_uri() .'/dist/style.css' );
// 	printf('<style>%s</style>', $css);
// });


/******************************************
*  			 A D M I N    E N Q U E U E
*******************************************/

add_action( 'admin_enqueue_scripts', 'admin_assets' );
function admin_assets(){
	wp_enqueue_script('theme-admin-script',        get_template_directory_uri() .'/js/admin_script.js');
	wp_enqueue_style('theme-admin-style', get_template_directory_uri() . '/css/admin_style.css');
	wp_enqueue_style( 'dashicons' );
}


add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );
function sdt_remove_ver_css_js( $src, $handle ) {
	$handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

	if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
		$src = remove_query_arg( 'ver', $src );

	return $src;
}
