<?php
add_action('init', 'otc_add_post_types' );
function otc_add_post_types(){
    register_post_type('strategy', // Register Custom Post Type
        array(
        'labels' => array(
            'name'          => __('Стратегии'), // Rename these to suit
            'singular_name' => __('Стратегия'),
        ),
        'public' 					=> true,
        'menu_icon'  			=> 'dashicons-chart-line',
        'hierarchical' 		=> false, // Allows your posts to behave like Hierarchy Pages
        'has_archive' 		=> false,
        'supports' => array('title', 'thumbnail', 'editor' ), // Go to Dashboard Custom HTML5 Blank post for supports
    ));


		register_taxonomy( 'lesson_level', [ 'lesson' ], [
			'label'             => 'Уровень Урока', // определяется параметром $labels->name
			'labels'            => [
				'name'            => 'Уровень Урока',
				'singular_name'   => 'Уровень Урока',
			],
			'description'       => 'Уровень Урока', // описание таксономии
			'public'            => true,
			'hierarchical'      => false,
			'meta_box_cb'				=> 'post_categories_meta_box',
			'rewrite'           => true,
		] );


		register_post_type('lesson', // Register Custom Post Type
        array(
        'labels' => array(
            'name'          => __('Уроки'), // Rename these to suit
            'singular_name' => __('Уроки'),
        ),
        'public' 					=> true,
        'menu_icon'  			=> 'dashicons-welcome-learn-more',
        'hierarchical' 		=> false, // Allows your posts to behave like Hierarchy Pages
        'has_archive' 		=> false,
        'supports' => array('title', 'thumbnail', 'editor' ), // Go to Dashboard Custom HTML5 Blank post for supports
    ));

}
