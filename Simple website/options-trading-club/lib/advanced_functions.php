<?php
// ini_set( 'display_errors', 1 );

// add_action('init', 'remove_pages_from_search');
function remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}

/**
 * Search Query Ordering
 */
add_action('pre_get_posts', 'search_query_ordering');
function search_query_ordering ($query) {
    if( !is_admin() && isset($query->query_vars['s']) && !empty($query->query_vars['s']) ){
        $query->set( 'orderby', 'date' );
        $query->set( 'order', 'DECS' );
	}
}

/**
 * Sitemap  reWrite
 */

add_action('init', 'do_rewrite_sitemap');
function do_rewrite_sitemap(){
	global $wp_rewrite;

	add_rewrite_rule('sitemap(-[a-z0-9_\-]+)?\.([0-9]+\.)?xml$', $wp_rewrite->index . '?feed=sitemap$matches[1]&m=$matches[2]', 'top');
	add_rewrite_rule('sitemap-news\.xml$', $wp_rewrite->index . '?feed=sitemap-news', 'top');

	add_rewrite_rule('sitemap(\-[a-z0-9_\-]+)?(\.[0-9]+)?\.xml(\.gz)?$', $wp_rewrite->index . '?feed=sitemap$matches[1]$matches[3]&m=$matches[2]', 'top' );
	add_rewrite_rule('sitemap(-[a-z0-9_\-]+)?\.([0-9]+\.)?xml$', 'index.php?feed=sitemap$matches[1]&m=$matches[2]', 'top');
}

/**
 * 		IF try to save ONLY *.php files via WP Dashboard Editor
 * 		we get issue  "cURL error 60: Peer's Certificate has expired."
 *
 * 		Filter below normalize this.
 */
add_filter( 'https_ssl_verify', '__return_false', 90 );
