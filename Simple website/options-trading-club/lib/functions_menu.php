<?php

#=============================================
#       FUNCTIONS of render menu(s)

function render_menu($menu_location = false)
{
	$MenuHTML = wp_nav_menu(array(
		'theme_location'  => $menu_location,
		'menu_class'      => 'menu ' . $menu_location,
		'echo'            => false,
		'container'       => '',
		'items_wrap'      => '<nav class="nav-menu--' . $menu_location . '"><ul class="%2$s">' . $add_li . '%3$s</ul></nav>',
		'depth'           => 0,
		'walker'          => new B20_Main_Menu_Walker(),
	));
	echo $MenuHTML;
}


#=============================================
#       Init menu(s)

add_action('after_setup_theme', 'theme_register_nav_menu');
function theme_register_nav_menu()
{
	register_nav_menu('main-menu', 	'Main Menu');
	register_nav_menu('mob-menu', 	'Mobile Menu');
	register_nav_menu('footer-menu', 	'Footer Menu');
}


#=============================================
#       WALKERS


class B20_Main_Menu_Walker extends Walker_Nav_Menu
{

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
	{
		//global $wp_query;
		$indent = ($depth > 0 ? str_repeat("\t", $depth) : ''); // code indent
		$show_more = '';

		// depth dependent classes
		$depth_classes = array(
			($depth == 0 ? 'main-menu-item' : 'sub-menu-item'),
			($depth >= 2 ? 'sub-sub-menu-item' : ''),
			($depth % 2 ? 'menu-item-odd' : 'menu-item-even'),
			'menu-item-depth-' . $depth
		);
		$depth_class_names = esc_attr(implode(' ', $depth_classes));

		// passed classes
		$classes = empty($item->classes) ? array() : (array) $item->classes;
		$class_names = esc_attr(implode(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item)));
		$cat_name       = (is_category(esc_attr($item->attr_title))) ? 'category' : '';

		// build html
		$output .= $indent . '<li id="mobile-nav-menu-item-' . $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . ' ' . $cat_name . '">';

		$args = isset($args) ? $args : array();

		// transform to amp-url
		if ($item->type == 'custom') {
			$url_amp = $item->url;
		} else {
			$url_amp = $item->url;

			if (get_query_var('amp') === 'amp') {
				$url_amp = transform_to_amp_url($item->url);
			}
		}

		// link attributes
		$attributes  = !empty($item->title)      ? ' title="'  . esc_attr($item->title) . '"' : '';
		$attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
		$attributes .= !empty($item->url)        ? ' href="'   . esc_attr($url_amp) . '"' : '';
		//$attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

		$item_output = sprintf(
			'%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$show_more,
			$args->link_before,
			apply_filters('the_title', $item->title, $item->ID),
			$args->link_after,
			$args->after
		);

		// build html
		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
	}
}


#=============================================
#       OTHER

// Add atribute title to menu link if notexist or empty
add_filter('nav_menu_link_attributes', function ($atts, $item, $args) {

	if (!isset($atts['title']) || empty($atts['title'])) {
		$atts['title'] = esc_attr($item->title);
	}

	return $atts;
}, 90, 3);
