/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/scripts.js":
/*!***********************!*\
  !*** ./js/scripts.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($, root, undefined) {
  var do_msrc = false;
  $(document).ready(function () {
    var _body = $('body');

    var $slider = $('[data-slider]');
    $slider.on('init', function () {
      $slider.addClass('slider--ready');
      setTimeout(function () {
        $slider.removeClass('slider--ready');
        $slider.removeClass('slider__pre-init');
      }, 2350);
    });
    $slider.slick({
      dots: true,
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg role="img" viewBox="0 0 256 512"><path fill="currentColor" d="m231.3 473.9 19.8-19.8a12 12 0 0 0 0-17L70.4 256 251 74.9a12 12 0 0 0 0-17l-19.8-19.8a12 12 0 0 0-17 0L5 247.5a12 12 0 0 0 0 17l209.4 209.4a12 12 0 0 0 17 0z"/></svg></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><svg viewBox="0 0 256 512"><path fill="currentColor" d="M24.7 38.1 5 57.9a12 12 0 0 0 0 17L185.6 256 5 437.1a12 12 0 0 0 0 17l19.8 19.8a12 12 0 0 0 17 0L251 264.5a12 12 0 0 0 0-17L41.7 38.1a12 12 0 0 0-17 0z"/></svg></button>'
    });
    /* What need to do while doc scrolling */

    var WScr = $(document).scrollTop();
    var DocScroll = WScr; // stick

    if (WScr > 300) {
      $('body').addClass('do_sticky');
    } else {
      $('body').removeClass('do_sticky');
    }

    $(document).on('scroll', function () {
      // stick
      DocScroll = WScr = $(document).scrollTop();
      if (WScr > 200) $('body').addClass('do_sticky');else $('body').removeClass('do_sticky');
    });
    $('#goto_top').on('click', function (e) {
      $("html, body").animate({
        scrollTop: 0
      }, 1000);
    });
    /*******************************************/

    $(window).on('resize', function () {
      if (window.innerWidth >= 1025) {
        document.getElementById("toggle-mm").checked = false;
      }
    }); // $(document).mouseup(function (e) {
    //     var container = $('#drop-down-menu');
    //     if (container.has(e.target).length === 0){
    //             container.hide();
    //         }
    // });

    $('.js-remove-style').removeAttr('style').removeClass('js-remove-style');
  });
  $(window).load(function () {
    /* Simple LazyLoad */
    $('img[data-src]').each(function (i, e) {
      $(e).attr('src', $(e).data('src'));
    });
  });
})(jQuery, this);

/***/ }),

/***/ "./scss/style.scss":
/*!*************************!*\
  !*** ./scss/style.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***********************************************!*\
  !*** multi ./js/scripts.js ./scss/style.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/redwek/Projects/Darkness/darkness.zp.ua/noosph/wp-content/themes/options-trading-club/js/scripts.js */"./js/scripts.js");
module.exports = __webpack_require__(/*! /home/redwek/Projects/Darkness/darkness.zp.ua/noosph/wp-content/themes/options-trading-club/scss/style.scss */"./scss/style.scss");


/***/ })

/******/ });