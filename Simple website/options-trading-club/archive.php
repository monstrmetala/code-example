<?php get_header();

	global $wp_query;

	$_date_in_title = '';
	if( isset($wp_query->query['year']) && !empty($wp_query->query['year']) ) {
		$_date_in_title = ': '.$wp_query->query['year'];

		if( isset( $wp_query->query['monthnum'] ) && !empty( $wp_query->query['monthnum'] ) ){
			$_date_in_title = ': ' . date('F, Y', strtotime( $wp_query->query['year'] .'/'. $wp_query->query['monthnum'] . '/01') );

			if( isset( $wp_query->query['day'] ) && !empty( $wp_query->query['day'] ) ){
				$_date_in_title = ': ' . date('F j, Y', strtotime( $wp_query->query['year'] .'/'. $wp_query->query['monthnum'] . '/' . $wp_query->query['day']) );
			}
		}
	}
	$head_title = 'Archive'.$_date_in_title ;

?>


<main id="main">
    <div class="row">
			<div class="loop_and_sbare">

				<div class="posts_in_loop">

					<h2 class="title-in-archive"><span><?php echo $head_title; ?></span></h2>

					<?php
						if (have_posts()) :
							get_template_part( 'loop' );
						endif;
					?>

					<div class="loop_nav">
						<div class="loop_nav_box">
							<?php
								$arg = array(
									'prev_text'          => __( '« Previous' ),
									'next_text'          => __( 'Next »' ),
									'screen_reader_text' => ' ',
								);
								echo get_the_posts_pagination( $arg );
							?>
						</div>
					</div>

					<div class="loop_nav">
						<div class="loop_nav_box">
							<?php
								$arg = array(
									'prev_text'          => __( '« Previous' ),
									'next_text'          => __( 'Next »' ),
									'screen_reader_text' => ' ',
								);
								echo get_the_posts_pagination( $arg );
							?>
						</div>
					</div>

				</div>

				<aside class="b20-sidebar">
					<?php dynamic_sidebar('main-sidebar'); ?>
				</aside>

        </div>
    </div>
</main>

<?php get_footer(); ?>
