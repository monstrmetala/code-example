<?php /* Template Name: Брокеры */
get_header();

?>
<main role="main" id="main">
<?php if (have_posts()): while (have_posts()) : the_post();	?>

	<?php
		$intro_bg = '';

		$post_thumbnail_id = get_post_thumbnail_id( $post );
		if ( $post_thumbnail_id ) {
			$intro_bg = wp_get_attachment_image_src($post_thumbnail_id, 'full');

			$intro_bg = sprintf( 'style="background-image: url(%s)"', $intro_bg[0] );
		}
	?>
	<section class="section-intro section-intro--brockers" <?php echo $intro_bg; ?> >
		<div class="row">
			<div class="user-html learning-intro-text "><?php the_content(); ?></div>
		</div>
	</section>

	<?php
		$brockers = get_field('brockers');

		foreach ($brockers as $brocker) {
			$br_thumb_html = '';
			if ($brocker['thumb']) {
				$br_thumb_html = sprintf('<div class="brocker__thumb"><img src="%s" alt="%s"></div>', $brocker['thumb']['sizes']['medium_large'], esc_attr($brocker['title']));
			}

			$br_btn_html = '';
			if ($brocker['reg_link']) {
				$br_btn_html = sprintf( '<a href="%s" title="РЕГИСТРАЦИЯ СЧЕТА" class="btn brocker__join">РЕГИСТРАЦИЯ СЧЕТА</a>', $brocker['reg_link'] );
			}

			?>
			<section class="brocker">
				<div class="row brocker__row brocker__row--desktop">
					<?php echo $br_thumb_html; ?>
					<div class="brocker__info">
						<h2 class="brocker__title"><?php echo $brocker['title']; ?></h2>
						<p class="brocker__descr"><?php echo $brocker['desct']; ?></p>
						<?php echo $br_btn_html; ?>
					</div>
				</div>
				<div class="row brocker__row brocker__row--mob">
					<div class="brocker__info">
						<h2 class="brocker__title"><?php echo $brocker['title']; ?></h2>
						<?php echo $br_thumb_html; ?>
						<p class="brocker__descr"><?php echo $brocker['desct']; ?></p>
						<?php echo $br_btn_html; ?>
					</div>
				</div>
			</section>
			<?php
		}
	?>


<?php endwhile; endif; ?>
</main>
<?php get_footer(); ?>
