<?php /* Template Name: График */
get_header();
?>
<main role="main" id="main">
	<?php if (have_posts()): while (have_posts()) : the_post();	?>
	<section class="home-graphic">
		<div class="row">
			<h2 class="section-title"><?php echo get_field('Title'); ?></h2>
			<?php render_graph(); ?>
		</div>
	</section>

	<?php endwhile; endif; ?>
</main>
<?php get_footer(); ?>
