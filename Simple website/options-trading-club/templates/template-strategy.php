<?php /* Template Name: Стратегии */
get_header();
?>
<main role="main" id="main">
	<div class="row">
		<section class="section-strategy">
			<?php
				$strat_list = get_field('strat_list');

				$strat_list = get_posts( [
					'numberposts' => -1,
					'orderby'     => 'date',
					'order'       => 'DESC',
					'post_type'   => 'strategy',
					'post_status' => 'publish'
				] );



				$strata_tpl = '<article class="grey-gradient strata">
					<div class="strata__content strata__content--desktop">
						<div class="strata__image">%5$s</div>
						<h2 class="strata__title"><a href="%1$s" title="%2$s" >%3$s</a></h2>
						%4$s
					</div>
					<div class="strata__content strata__content--mob">
						<h2 class="strata__title"><a href="%1$s" title="%2$s" >%3$s</a></h2>
						<div class="strata__image">%5$s</div>
						%4$s
					</div>
				</article>';



				foreach ($strat_list as $key => $strata) {

					$url = get_permalink( $strata->ID );
					$img_html = '';

					$post_thumbnail_id = get_post_thumbnail_id( $strata );
					if ( $post_thumbnail_id ) {
						$thumb = wp_get_attachment_image_src($post_thumbnail_id, 'large');
						$img_html = sprintf( '<img src="%s" alt="%s">' , $thumb[0], esc_attr($strata->post_title) );
					}

					$preview = kama_excerpt($strata, [
						'maxchar' => 800
					]);

					$content = $preview;

					# Render
					printf($strata_tpl,
						$url,
						esc_attr($strata->post_title),
						$strata->post_title,
						// $strata->post_content,
						$content,
						$img_html
					);
				}
			?>

		</section>
	</div>
</main>
<?php get_footer(); ?>
