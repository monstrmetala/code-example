<?php
  global $SVG;
?>

<input type="checkbox" id="toggle-mm" value="1" style="display: none;" class="checkbox-mm">
<div id="mm-wrp" class="mm js-remove-style" style="display: none;" >
	<div class="mm__box">

		<div class="mm__content">
			<label for="toggle-mm" class="mm__close-btn">Закрыть <?php echo $SVG['close'] ?></label>
			<?php render_menu('mob-menu'); ?>
		</div>

	</div>
	<label for="toggle-mm" class="mm__close-area"></label>
</div>

