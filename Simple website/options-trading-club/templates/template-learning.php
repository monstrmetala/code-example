<?php /* Template Name: Обучение */
	get_header();
	global $SVG;


?>
<main role="main" id="main">
	<?php if (have_posts()): while (have_posts()) : the_post();	?>
		<?php
			$intro_bg = '';

			$post_thumbnail_id = get_post_thumbnail_id( $post );
			if ( $post_thumbnail_id ) {
				$intro_bg = wp_get_attachment_image_src($post_thumbnail_id, 'full');

				$intro_bg = sprintf( 'style="background-image: url(%s)"', $intro_bg[0] );
			}
		?>
		<section class="section-intro" <?php echo $intro_bg; ?> >
			<div class="row">
				<div class="user-html learning-intro-text"><?php the_content(); ?></div>
			</div>
		</section>

		<section>
			<div class="row">
				<div class="learning">
					<?php
						foreach (['base' => 'НАЧАЛЬНЫЙ','pro'=>'ПРОДВИНУТЫЙ'] as $lvl => $name) {
							$img_html = '';
							$img = get_field( $lvl . '_img' );
							if ($img) {
								$img_html = sprintf( '<img src="%s" alt="%s">', $img['sizes']['large'], $name );
							}

							$text = get_field( $lvl . '_txt' );
							$lessons_list = get_field( 'lessons_list_'.$lvl, 'option' );

							$lesson_list_html = '';
							$lessons_list_li = [];
							if ($lessons_list) {
								foreach ($lessons_list as $lessons) {
									$lessons_list_li[] = sprintf(
										'<li><a href="%s" title="%s" >%s</a></li>',
										get_permalink( $lessons->ID ),
										esc_attr($lessons->post_title),
										$lessons->post_title
									);
								}
							}

							if (!empty($lessons_list_li)) {
								$lesson_list_html = sprintf( '<ul class="llevel__list">%s</ul>', implode('', $lessons_list_li) );
							}

							?>
							<div class="grey-gradient llevel">
								<div class="llevel__title">
									<span class="octa llevel__octa"><?php echo $SVG['octa'] ?><b><?php echo mb_substr($name, 0, 1); ?></b></span>
									<p class="llevel__name">
										<span class="name-part name-part--name"><?php echo $name; ?></span><br><span class="name-part name-part--level">УРОВЕНЬ</span>
									</p>
								</div>

								<div class="llevel__img"><?php echo $img_html; ?></div>
								<div class="llevel__descr"><?php echo $text; ?></div>
								<?php echo $lesson_list_html; ?>
							</div>
							<?php
						}
					?>
				</div>
			</div>
		</section>
	<?php endwhile; endif; ?>

</main>
<?php get_footer(); ?>
