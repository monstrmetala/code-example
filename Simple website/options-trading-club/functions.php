<?php
define('THEME_DATA_FORMAT', 'F j, Y');

include_once 'lib/svg-icons.php';
include_once 'lib/functions_default.php';
include_once 'lib/functions_enqueue.php';
include_once 'lib/functions_menu.php';
include_once 'lib/post_types.php';

include_once 'lib/functions-ajax.php';
include_once 'lib/widgets.php';
include_once 'lib/acf.php';

/**
 *      More includes ..
 */
include_once 'lib/advanced_functions.php';


/**
 * 	Rename Image before upload "new name" is md5( 'old name' )
 */
add_filter('sanitize_file_name', 'rename_img_before_upload', 99 );
function rename_img_before_upload( $filename ){
	$pi = pathinfo($filename);

	if(
		isset( $pi['filename'] ) && !empty( $pi['filename'] ) &&
		isset( $pi['extension'] ) && !empty( $pi['extension'] )
	){

		$new_filename = md5( $pi['filename'] );
		$new_filename .= '.'.$pi['extension'];
		return $new_filename;

	}else return $filename;
}

function render_graph () {
	the_field('graph_script', 'option');
}

function get_prev_next ( )
{
	$ID = get_the_ID();

	$prev_post = null;
	$next_post = null;

	$lessons_list_base = get_field( 'lessons_list_base', 'option');
	if ($lessons_list_base)
	foreach ($lessons_list_base as $key => $value) {

		if ($value->ID == $ID) {
			$prev_post = (isset($lessons_list_base[$key - 1])) ? $lessons_list_base[$key - 1] : null;
			$next_post = (isset($lessons_list_base[$key + 1])) ? $lessons_list_base[$key + 1] : null;
		}
	}

	$lessons_list_pro = get_field( 'lessons_list_pro', 'option');
	if ($lessons_list_pro)
	foreach ($lessons_list_pro as $key => $value) {

		if ($value->ID == $ID) {
			$prev_post = (isset($lessons_list_pro[$key - 1])) ? $lessons_list_pro[$key - 1] : null;
			$next_post = (isset($lessons_list_pro[$key + 1])) ? $lessons_list_pro[$key + 1] : null;
		}
	}

	$tpl = '<a class="leason-navi__item %s" href="%s" title="%s" ><b>%s</b><span>%s</span></a>';

	if ($prev_post) {
		$prev_post = sprintf($tpl,
			'leason-navi__item--prev',
			get_permalink($prev_post->ID),
			esc_attr($prev_post->post_title),
			'Предыдущий урок',
			$prev_post->post_title
		);
	}

	if ($next_post) {
		$next_post = sprintf($tpl,
			'leason-navi__item--next',
			get_permalink($next_post->ID),
			esc_attr($next_post->post_title),
			'Следующий урок',
			$next_post->post_title
		);
	}

	return [
		'prev' => $prev_post,
		'next' => $next_post
	];

}


function get_excerpt_max_charlength( $article, $charlength ){
	$excerpt = get_the_excerpt($article);
	$charlength++;

	ob_start();
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '[.!.]';
	} else {
		echo $excerpt;
	}

	return ob_get_clean();
}


function kama_excerpt( $article, $args = '' ){

	if( is_string( $args ) ){
		parse_str( $args, $args );
	}

	$rg = (object) array_merge( [
		'maxchar'           => 350,
		'text'              => '',
		'autop'             => true,
		'ignore_more'       => false,
		'save_tags'         => '<strong><b><a><em><i><var><code><span>',
		'sanitize_callback' => static function( string $text, object $rg ){
			return strip_tags( $text, $rg->save_tags );
		},
	], $args );

	$rg = apply_filters( 'kama_excerpt_args', $rg );

	if( ! $rg->text ){
		$rg->text = $article->post_excerpt ?: $article->post_content;
	}

	$text = $rg->text;
	// strip content shortcodes: [foo]some data[/foo]. Consider markdown
	$text = preg_replace( '~\[([a-z0-9_-]+)[^\]]*\](?!\().*?\[/\1\]~is', '', $text );
	// strip others shortcodes: [singlepic id=3]. Consider markdown
	$text = preg_replace( '~\[/?[^\]]*\](?!\()~', '', $text );
	// strip direct URLs
	$text = preg_replace( '~(?<=\s)https?://.+\s~', '', $text );
	$text = trim( $text );

	// <!--more-->
	if( ! $rg->ignore_more && strpos( $text, '<!--more-->' ) ){

		preg_match( '/(.*)<!--more-->/s', $text, $mm );

		$text = trim( $mm[1] );
	}
	// text, excerpt, content
	else {

		$text = call_user_func( $rg->sanitize_callback, $text, $rg );
		$has_tags = false !== strpos( $text, '<' );

		// collect html tags
		if( $has_tags ){
			$tags_collection = [];
			$nn = 0;

			$text = preg_replace_callback( '/<[^>]+>/', static function( $match ) use ( & $tags_collection, & $nn ){
				$nn++;
				$holder = "~$nn";
				$tags_collection[ $holder ] = $match[0];

				return $holder;
			}, $text );
		}

		// cut text
		$cuted_text = mb_substr( $text, 0, $rg->maxchar );
		if( $text !== $cuted_text ){

			// del last word, it not complate in 99%
			$text = preg_replace( '/(.*)\s\S*$/s', '\\1...', trim( $cuted_text ) );
		}

		// bring html tags back
		if( $has_tags ){
			$text = strtr( $text, $tags_collection );
			$text = force_balance_tags( $text );
		}
	}

	// add <p> tags. Simple analog of wpautop()
	if( $rg->autop ){

		$text = preg_replace(
			[ "/\r/", "/\n{2,}/", "/\n/" ],
			[ '', '</p><p>', '<br />' ],
			"<p>$text</p>"
		);
	}

	$text = apply_filters( 'kama_excerpt', $text, $rg );

	return $text;
}
