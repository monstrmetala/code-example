(function ($, root, undefined) {
	var do_msrc = false;

	$(document).ready(function() {
		var _body = $('body');

		let $slider = $('[data-slider]');
		$slider.on('init', () => {
			$slider.addClass('slider--ready');
			setTimeout(()=>{
				$slider.removeClass('slider--ready');
				$slider.removeClass('slider__pre-init');
			}, 2350);
		});


		$slider.slick({
			dots: true,
			prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg role="img" viewBox="0 0 256 512"><path fill="currentColor" d="m231.3 473.9 19.8-19.8a12 12 0 0 0 0-17L70.4 256 251 74.9a12 12 0 0 0 0-17l-19.8-19.8a12 12 0 0 0-17 0L5 247.5a12 12 0 0 0 0 17l209.4 209.4a12 12 0 0 0 17 0z"/></svg></button>',
			nextArrow: '<button type="button" class="slick-arrow slick-next"><svg viewBox="0 0 256 512"><path fill="currentColor" d="M24.7 38.1 5 57.9a12 12 0 0 0 0 17L185.6 256 5 437.1a12 12 0 0 0 0 17l19.8 19.8a12 12 0 0 0 17 0L251 264.5a12 12 0 0 0 0-17L41.7 38.1a12 12 0 0 0-17 0z"/></svg></button>'
		});

		/* What need to do while doc scrolling */
		var WScr = $(document).scrollTop();
		var DocScroll = WScr;
		// stick
		if (WScr > 300) {
			$('body').addClass('do_sticky');

		} else {
			$('body').removeClass('do_sticky');
		}

		$(document).on('scroll', function () {

				// stick
				DocScroll = WScr = $(document).scrollTop();
				if( WScr > 200)
						$('body').addClass('do_sticky');
				else
						$('body').removeClass('do_sticky');

		});

		$('#goto_top').on('click', function(e) {
				$("html, body").animate({
						scrollTop: 0
				}, 1000);
		});

		/*******************************************/


		$(window).on('resize', () => {
			if (window.innerWidth >= 1025) {
				document.getElementById("toggle-mm").checked = false;
			}
		});

			// $(document).mouseup(function (e) {
				//     var container = $('#drop-down-menu');
				//     if (container.has(e.target).length === 0){
					//             container.hide();

					//         }
					// });

		$('.js-remove-style').removeAttr('style').removeClass('js-remove-style');
	});

	$(window).load( ()=>{
			/* Simple LazyLoad */
			$('img[data-src]').each((i,e)=>{ $(e).attr('src', $(e).data('src') ); });
	});

})(jQuery, this);
