<?php global $SVG; ?>

<!-- search -->
<form class="search b20s in_header" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" placeholder="Search ...">
	<button class="search-submit" type="submit" role="button" title="Search"><?php echo $SVG['search']; ?></button>
</form>
<!-- /search -->
