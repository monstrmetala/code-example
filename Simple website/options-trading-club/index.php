<?php get_header();
$page_for_posts = get_option( 'page_for_posts' );

?>
<main role="main" class="mt70">

	<!-- section -->
		<div class="row row_in_page">

			<div class="post_content">

				<div class="main_article category_box">
					<?php
					echo '<header><h1 class="title">'.get_the_title($page_for_posts).'</h1></header>';

					if (have_posts()) :

						// Get Template
						echo '<div class="cat-layout">';
						get_template_part( 'loop' );
						echo '</div>';

						// Get Pagination
						if ($wp_query->max_num_pages > 1) : ?>
						<div class="post-pagination clearfix">
							<span class="pagi-btn previous"><?php previous_posts_link( __( $SVG['arrow_prev'].'Previous page') ) ?></span>
							<span class="pagi-btn next"><?php next_posts_link( __('Next page'.$SVG['arrow_next']) ) ?></span>
						</div>
						<?php endif;
					else :
						// No Posts
						esc_html_e('No posts.', 'newsgamer');
					endif;

					?>
				</div>

				<aside class="post_aside">
					<div id="sidebar" class="sidebar">
						<div class="theiaStickySidebar">
							<?php dynamic_sidebar( 'primary-widget-area' ); ?>
						</div>
					</div>
				</div>

			</div>

		</div>
</main>

<?php get_footer(); ?>
