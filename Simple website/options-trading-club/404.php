<?php
	get_header();
	global $SVG;
?>

<main id="main">
    <div class="row">

			<div class="box404">
				<span class="octa box404__ocata-border"><?php echo $SVG['octa'] ?></span>
				<p>4<span class="octa box404__ocata-zero"><?php echo $SVG['octa'] ?></span>4</p>
			</div>

			<h2 class="no-page">Страница не найдена</h2>


    </div>
</main>


<?php
    // load footer
    get_footer();
?>
