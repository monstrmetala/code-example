let mix = require('laravel-mix');

mix
    .js('js/scripts.js', 'dist/scripts.js')
    .sass('scss/style.scss', 'dist/style.css')
    .options({
        processCssUrls: false
    });

/*
mix.browserSync({
    proxy: 'mtg-wc.loc',
    files: ['*.php', 'views/!**!/!*.php', 'dist/app.css', 'dist/app.js']
});*/
