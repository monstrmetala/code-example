<?php
/**
 * The template for displaying Comments
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}


?>

<!-- start:article-comments -->
<section id="comments">

	<?php if ( have_comments() ) : ?>
	<header>
		<h2 class="h6 title">
			<span><?php printf( _n( '1 Comment', '%1$s Comments', get_comments_number() ), number_format_i18n( get_comments_number() ) ); ?></span>
		</h2>
	</header>

	<ol id="comments-list">
		<?php
	    wp_list_comments( array(
			'style' => 'ol',
			'short_ping' => true,
			'avatar_size' => 75,
			'callback' => 'b20_comments_callback',
	    ) );
		?>
	</ol><!-- .comment-list -->

	<div class="comment-pagination">
		<?php paginate_comments_links(); ?>
	</div>

	<?php endif; // have_comments() ?>

</section>
<!-- end:article-comments -->

<!-- start:article-comments-form -->
<section id="article-comments-form">

	<?php

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

        $fields =  array(
			'author' =>
				'<div class="w50 form-control-wrp">
					<input class="form-control" id="author" name="author" placeholder="' . esc_html__('Name:') . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
				'</div>',

			'email'  =>
				'<div class="w50 form-control-wrp">
					<input class="form-control" id="email" name="email" placeholder="' . esc_html__('Email:') . '" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' .
				'</div>',
		);

	$defaults = array(
		'fields' => apply_filters('comment_form_default_fields', $fields ),
	);

	$defaults['comment_field'] = '<div class="form-control-wrp">
			<textarea class="form-control " placeholder="' . esc_html__('Comment:') . '" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
		</div>';



	$defaults['comment_notes_before'] = '';
	$defaults['comment_notes_after']  = '';
	$defaults['title_reply_before']   = '<h6 class="comment-reply-title">';
	$defaults['title_reply_after']    = '</h6>';
	$defaults['title_reply'] = esc_html__('Leave a Reply');


	$defaults['label_submit'] = esc_html__('Post Comment');
	$defaults['cancel_reply_link'] = esc_html__('Cancel Comment');

	$defaults['logged_in_as'] = '<p class="logged-in-as">' . sprintf( __( '<a href="%1$s" title="Your Profile">Logged in as %2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( get_the_ID() ) ) ) ) . '</p>';
	

	comment_form($defaults);
	?>

</section>
<!-- end:article-comments-form -->
<?php




/**
* Custom callback for outputting comments
*/

function b20_comments_callback( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	if ( $comment->comment_approved == '1' ) {
		global $SVG;
		?>
		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
			<header class="comment-header">
				<?php echo get_avatar( $comment->comment_author_email ); ?>

				<div class="comment-info">
					<p class="pull-left"><i><?php comment_author_link() ?></i> says:</p>
					<span class="time-stamp">
						<?php comment_date() ?> <?php esc_html_e('at') ?> <?php comment_time() ?>
					</span>
				</div>
			</header>

			<?php comment_text(); ?>

			<div class="replay-comment">
				<?php
					comment_reply_link(array_merge( $args, array(
						'depth' => $depth,
						'max_depth' => $args['max_depth'],
						'reply_text' => esc_html__('Reply'),
						'login_text' =>  esc_html__('Log in to leave a comment'),
						'before' => '<span class="reply pull-right">',
						'after' => '</span>'
					)))
				?>
			</div>
		</li>
		<?php
	}
}
