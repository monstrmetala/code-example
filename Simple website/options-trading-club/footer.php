		<?php /*/ ?>
		<button id="goto_top" >
			<svg role="img" viewBox="0 0 320 512" style="height: 15px;"><path fill="currentColor" d="M177 159.7l136 136c9.4 9.4 9.4 24.6 0 33.9l-22.6 22.6c-9.4 9.4-24.6 9.4-33.9 0L160 255.9l-96.4 96.4c-9.4 9.4-24.6 9.4-33.9 0L7 329.7c-9.4-9.4-9.4-24.6 0-33.9l136-136c9.4-9.5 24.6-9.5 34-.1z"/></svg>
		</button>
		<?php /*/ ?>


		<footer id="theme_footer" class="footer" role="contentinfo">

			<div class="row">

				<div class="footer__middle">
					<?php
						$logo_img = get_field('logo_img', 'option');
						$logo_url = get_field('logo_url', 'option');

						if ($logo_img) {
							$flogo = sprintf( '<img src="%s" alt="Footer">', $logo_img['url'] );

							if ($logo_url) {
								printf( '<a class="footer__logo" href="%s" title="Logo" >%s</a>', $logo_url, $flogo );
							} else {
								printf( '<span class="footer__logo" title="Logo" >%s</span>', $flogo );
							}
						}
					?>
				</div>

				<div class="footer__bottom">
					<?php
						$seo_text = get_field('seo_text', 'option');
						echo ($seo_text) ? "<p>$seo_text</p>" : '';
					?>

					<div class="copyright">
						<?php
							$FCR = get_field('copyright', 'option');
							if( $FCR ){
								echo str_replace( '{year}', date('Y'), $FCR );
							}else
							printf(
								"© %s <a htef='%s' title='%s'>%s</a> — All rights reserved",
								date('Y'),
								get_bloginfo('url'),
								get_bloginfo('name'),
								get_bloginfo('name')
							);
						?>
					</div>

				</div>
			</div>

		</footer>

		<?php wp_footer(); ?>

	</body>
</html>
