<?php get_header();
	$head_title = 'Tag: '.single_tag_title( '', false ) ;
?>

<main id="main">
	<div class="row">
		<div class="loop_and_sbare">

			<div class="posts_in_loop">
					<h2 class="title-in-archive"><span><?php echo $head_title; ?></span></h2>

					<?php
							if (have_posts()) :
									get_template_part( 'loop' );
							endif;
					?>

					<?php
						$prev_post = get_previous_posts_page_link();
						$next_post = get_next_posts_page_link();
						global $footer_pagi;

						if($prev_post) $footer_pagi['prev'] = [
							'link' => get_permalink( $prev_post->ID ),
							'title' => 'Previous Page',
						];

						if($next_post) $footer_pagi['next'] = [
							'link' => get_permalink( $next_post->ID ),
							'title' => 'Next Page',
						];
					?>

			</div>

			<aside class="b20-sidebar">
				<?php dynamic_sidebar('main-sidebar'); ?>
			</aside>

		</div>
	</div>
</main>

<?php get_footer(); ?>
