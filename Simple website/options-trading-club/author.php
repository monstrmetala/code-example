<?php get_header();
	// global $SVG;
	$curauth            = (get_query_var('author_name'))                                        ? get_user_by('slug', get_query_var('author_name'))                     : get_userdata(get_query_var('author'));
?>

<main id="main">
	<div class="row">
		<div class="loop_and_sbare">

			<div class="posts_in_loop">
				<h2 class="title-in-archive"><span>Author: <?php echo $curauth->data->display_name; ?></span></h2>

				<?php
					if (have_posts()) :
						get_template_part( 'loop' );
					endif;
				?>

			</div>

			<aside class="b20-sidebar">
				<?php dynamic_sidebar('main-sidebar'); ?>
			</aside>

		</div>
	</div>
</main>

<?php get_footer(); ?>
