<?php
	get_header();
?>
<main role="main" class="mt70">
	<!-- section -->
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<section class="section-post">
			<div class="row">
				<article class="user-html article">
					<h1 class="article__title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</article>
			</div>
		</section>

	<?php endwhile; ?>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
